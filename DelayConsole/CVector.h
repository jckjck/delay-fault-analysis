#pragma once 


// CUSTOM HEADERS
#include "CGraph.h"
#include "CDataExtractor.h"
#include "ElementTestInfo.h"
#include "Timer.h"
#include <ctime>

// STL HEADERS 
#include <string>
#include <vector>
#include <map>
#include <set>
#include <array>
#include <functional>
#include <memory>
#include <iostream>
#include <cstring>
#include <utility>
#include <algorithm>
#include <functional>
#include <numeric>

typedef unsigned int UINT;
typedef std::map< std::string, std::vector< std::string > > pairT;

class CVector
{

private:

/* VARIABLES */

	// PRMITIVES
	UINT m_Inputs;
	UINT m_Outputs;
	UINT m_Length;
	UINT m_foundPairs;
	UINT finalCount;
	UINT curElement;
	UINT pairCount;
	int m_curIndex;

	// CONTAINERS
	std::vector< std::string > m_Patterns;
	std::vector< std::string > m_Table;
	std::vector< std::string > elemOuts;
	std::vector< std::unique_ptr< CElementTestInfo > > elements;
	std::vector< std::unique_ptr< CElementTestInfo > > tested;
	std::string m_Cur;
	std::string m_Outs;
	std::string cover;
	std::vector< std::set< UINT > > m_UntestedElem;
	std::map< std::string, std::vector< std::string > > pairMap;
	std::set< UINT > m_UntestedInputs;
	std::map< UINT, UINT > revInputMap;
	std::map< std::string, int > elemName2Num;

	// MEMBER OBJECTS
	std::unique_ptr< CDataExtractor > dxIn;
	std::unique_ptr< CDataExtractor > dxOut;

	// BOOLEAN FILTERS

	bool not;


/* METHODS */

	void SimulateAll( std::string& prStr, std::vector< CGraph >& graphs );

	// Process pairs read from second Devadze output
	void ProcessPairInputs( std::map< UINT, std::vector< UINT > >& fanouts, std::vector< Timer >& timers );

	// Process initial .tst-file to extract out pairs for the rest of untested elements
	void ProcessInitialTests( std::vector< CDataExtractor::TIMERINFO >& timers );

	// Calculate cover for delay faults
	void PrintCover( void );

	void PrintCoverCSV( void );

	void PrepareCover( void );

	void InsertToCover( const char& ch, const UINT& pos );

	// Scan vector pairs for element by element 
	void GetFaultsByElement( std::map< UINT, std::vector< UINT > >& fanouts );

	// Run the particular element test
	void RunElementTest( std::map< UINT, std::vector< UINT > >& fanouts, const unsigned test );


public:

/* METHODS */

	// Construct/Destruct

	CVector( UINT ins, UINT outs, UINT len )		
	{	
		m_Inputs = ins;
		m_Outputs = outs;
		m_Length = len;
		m_curIndex = -1;
	};

	~CVector( void ){};

	// Getters

		// 0 - get reference to the pattern vector
		// 1 - get reference to the table vector
	inline std::vector< std::string >& GetVecAccess( int type ) { return type ? m_Table : m_Patterns; };
	inline std::vector< std::unique_ptr< CElementTestInfo > >& GetTestArray( void ) { return elements; };
	const UINT GetInCnt( void ) const { return m_Inputs; };


	// Setters

	// Custom		m_Length	<Unable to read memory>	

	UINT GeneratePairSet( std::map< UINT, std::vector< UINT > >& mp, 
		pairT& pairs, std::vector< CGraph >& graphs );

	// Forms result analysis of vector pairs, based on 
	// rule (0/1 > 0, 1/0 > 1, x any other cases)
	void GeneratePairAnalysis( const std::string& source, const std::string& result, 
		std::map< UINT, std::vector< UINT > >& fanouts, std::vector< Timer >& timers );

	// Make second vector "worse" by flipping all the bits, where the first
	// vector has SAFs discovered
	void FlipSecodVectorBits( const std::string& source );

	void MergeVectors( const pairT& pairs, std::vector< std::string >& merged,
		const UINT& looseBits, std::vector< CGraph >& graphs );

};
