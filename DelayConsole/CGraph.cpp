// CGraph.cpp : implementation file
// Created by Jaak K�usaar @08.2013

#include "CNode.h"
#include "CDataExtractor.h"
#include "CGraph.h"

using std::vector;
using std::string;
using std::unique_ptr;
using std::shared_ptr;

// CGraph


//CNode* CGraph::PopNodeStack( void )
//{
//	CNode* ptr = m_ConNodes.top();
//	m_ConNodes.pop();
//
//	return ptr;
//}


//void CGraph::ResizeGraph( UINT size )
//{
//	m_Graph.resize( size );
//
//	for( UINT u = 0; u < size; ++u )
//	{
//		m_Graph[ u ] = new CNode();
//	}
//}


//void CGraph::PushNodeStack( CNode* ptr )
//{
//	m_ConNodes.push( ptr );
//}


void CGraph::SetNodeValues( const int mode, const string& values )
{
	for( auto i = m_Graph.begin(); i != m_Graph.end(); ++i )
	{
		char tc = values[ ( *i ).GetVarNum() ];
		// If value is high or "don't care" 
		if( tc == 'h' || tc == 'H' || tc == '1' || tc == 'x' )
		{
			// Set node value to 1
			( *i ).m_Val = 1;
			// If not fault free sim, check for inversion
			if( mode ) ( *i ).ToggleInversion();
		}
		else
			// Else set to zero
			( *i ).m_Val = 0;
	}
}


const int CGraph::FaultFreeSimul( vector< CNode >& nods ) const
{
	//if( !cn->IsInverted() )
	//{
	//	if( cn->IsOne() )
	//		return ( !cn->GetRChild() ? 1 : FaultFreeSimul( cn->GetRChild() ) );
	//	else 
	//		return ( !cn->GetDChild() ? 0 : FaultFreeSimul( cn->GetDChild() ) );
	//}
	//else
	//{
	//	if( cn->IsOne() )
	//		return ( !cn->GetDChild() ? 0 : FaultFreeSimul( cn->GetDChild() ) );
	//	else 
	//		return ( !cn->GetRChild() ? 1 : FaultFreeSimul( cn->GetRChild() ) );
	//}
	////throw unknown error
	CNode cur = nods[ 0 ];

	while( true )
	{
		if( !cur.IsInverted() )
		{
			if( cur.IsOne() )
			{
				if( !cur.GetRChild() )  return 1;
				else cur = nods[ cur.GetRChild() ];
			}
			else
			{
				if( !cur.GetDChild() ) return 0;
				else cur = nods[ cur.GetDChild() ];
			}
		}
		else
		{
			if( cur.IsOne() )
			{
				if( !cur.GetDChild() ) return 0;
				else cur = nods[ cur.GetDChild() ];
			}
			else
			{
				if( !cur.GetRChild() ) return 1;
				else cur = nods[ cur.GetRChild() ];
			}
		}
	}
}



CNode CGraph::GetNode( UINT num )
{
	return m_Graph[ num ];
}





