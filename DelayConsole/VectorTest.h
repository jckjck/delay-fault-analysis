#pragma once

#include "windows.h"
#include <vector>
#include <bitset>

class CVectorTest
{

public:

	static CVectorTest* SingletonInst();
	std::vector< std::bitset< 8 > >&   Generate8BitVectors( const int count );
	std::vector< std::bitset< 16 > >&  Generate16BitVectors( const int count );
	std::vector< std::bitset< 32 > >&  Generate32BitVectors( const int count );
	std::vector< std::bitset< 64 > >&  Generate64BitVectors( const int count );
	std::vector< std::bitset< 128 > >& Generate128BitVectors( const int count );
	~CVectorTest( void );

protected:

	CVectorTest();
	CVectorTest( const CVectorTest& );
	CVectorTest& operator= ( const CVectorTest& );

private:

	static CVectorTest*  pInst;
	std::vector< std::bitset< 8 > >	  m_vec8bit;
	std::vector< std::bitset< 16 > >  m_vec16bit;
	std::vector< std::bitset< 32 > >  m_vec32bit;
	std::vector< std::bitset< 64 > >  m_vec64bit;
	std::vector< std::bitset< 128 > > m_vec128bit;

	//METHODS
	const ULONG GenerateRandomBits( void );
};

