#pragma once

// STL HEADERS 
#include <string>
#include <vector>
#include <memory>
#include <iostream>
#include <algorithm>
#include <map>

// CUSTOM HEADERS
#include "Helper.h"

typedef unsigned int UINT;

class CElementTestInfo
{

private:

/* VARIABLES */

	// Primitives

	UINT outPut;
	UINT graphNum;
	bool isTested;
	bool isRobustTested;

	// Containers

	std::string typeName;
	 // These vector map input numbers, which are global for entire schema.
	 
	// Numbers that represent places in testvector for element input values
	std::vector< UINT > testNumbers;
	std::vector< UINT > test2AND;
	// Numbers that represent places in faultvector (TABLE) for every input
	std::vector< UINT > faultNumbers;
	std::vector< char > testedInputs;
	std::vector< char > codedInputs;


/* METHODS */

	// CUSTOM

	// Method that tests AND class element
	const bool TestAndClass( const std::string& S1, std::string& cov, 
		std::map< UINT, std::vector< UINT > >& fanouts );

	// Method that tests OR class element
	const bool TestOrClass( const std::string& S1, std::string& cov, 
		std::map< UINT, std::vector< UINT > >& fanouts );

	// Method that tests AOI class element
	const bool TestAOIClass( const std::string& S1, std::string& cov, 
		std::map< UINT, std::vector< UINT > >& fanouts );

	const bool TestNOT( const std::string& S1, std::string& cov,
		std::map< UINT, std::vector< UINT > >& fanouts );

	const bool TestOUT( const std::string& S1, std::string& cov );

	// Method for encoding input values to get algebra elements
	void EncodeInputs( std::string& S1, std::string& S2 );

	// Method for calculating AND gate fault types according to 7 - valued algebra
	const char CalculateANDType( const char c1, const char c2 );

	// Method for calculating OR gate fault types according to 7 - valued algebra
	const char CalculateORType( const char c1, const char c2 );

	// Method for detecting AND gate faults according to 7 - valued algebra
	const char DetectANDFault( const char c1, const char c2 );

	// Method for detecting OR gate faults according to 7 - valued algebra
	const char DetectORFault( const char c1, const char c2 );

	void Encode2ANDInputs( std::string& S1, std::string& S2 );

	const char FindAOI2AND( const char c1, const char c2 );

	const char FindAOI2OR( const char c1, const char c2 );

	const bool IsOutputTested( const std::string& R2 ) const;

	const bool IsFunctionalOutput( const std::string& R1 ) const;

	const char GetInheritedValue( std::map< UINT, std::vector< UINT > >& fanouts, std::string& cov );

	void EvaluateOutputs( const UINT indx, const char& inter, const char& inh, 
		const std::string& S1, std::string& cov, const bool and );


public:

/* METHODS */

	// CONSTRUCT/DESTRUCT

	CElementTestInfo( void ) 
	{ 
		isTested = false; 
		isRobustTested = false; 
		graphNum = 0;
	};
	// Copy constructor and copy assignment operator
	CElementTestInfo( const CElementTestInfo& orig );
	CElementTestInfo& operator= ( const CElementTestInfo& c );
	// Move constructor and move assignment operator
	CElementTestInfo( CElementTestInfo&& orig );
	CElementTestInfo& operator= ( CElementTestInfo&& orig );
	// Destructor
	~CElementTestInfo( void );

	// GETTERS

	inline const std::string GetTypeName( void ) const { return typeName; };
	inline const UINT		 GetGraphNum( void ) const { return graphNum; };
	inline const UINT	GetInput ( UINT u ) const { return faultNumbers.at( u ); }; 
	inline const UINT	GetTest	( UINT u ) const { return testNumbers.at( u ); };
	inline const char	GetTestInfo( UINT u ) const { return testedInputs.at( u ); };
	inline const UINT	GetInputSIze( void ) const { return faultNumbers.size(); };
	inline const UINT	GetTestSize( void ) const { return testNumbers.size(); };
	inline const UINT   GetOutput( void ) const { return outPut; };


	// SETTERS

	inline void SetGraphNum( UINT val ) { graphNum = val; };
	const UINT SetSingleInput( UINT u, UINT val ); 
	inline void SetInput( UINT u ) { faultNumbers.push_back( u ); }; 
	inline void SetTest( UINT u ) { testNumbers.push_back( u ); };
	inline void SetTested ( UINT u ) { testedInputs.assign( u, '0' ); };
	inline void SetTypeName( std::string nam ) { typeName = nam; };
	inline void SetOutput( UINT u ) { outPut = u; };

	// CUSTOM METHODS

	// Method that checks the type of gate and calls according test function
	const bool TestElement( const std::string& S1, std::string& cov,
		std::map< UINT, std::vector< UINT > >& fanouts );

	void CombineElementInputs( std::string& S1, std::string& S2, std::string& res );

	const bool IsTested( void ) const;

};


