#include "stdafx.h"
#include "CFileRoutines.h"

#include <fstream>
using std::streamoff;

#include <algorithm>
using std::replace;

using std::map;
using std::string;


bool CFileRoutines::IsGoodExt( const string& file, const string& ext )
{
	string s = file.substr( file.find_last_of( "." ) + 1 );
	UINT sz = s.size();

	if( ext.size() != sz )
	{
		AfxMessageBox( CString( "Wrong file extension length!" ) );
		return false;
	}

	for( size_t i = 0; i < sz; ++i )
	{
		if ( tolower( s[ i ] ) != tolower( ext[ i ] ) )
		{
			AfxMessageBox( CString( "Wrong file extension!" ) );
			return false;
		}
	}

	return true;
};

bool CFileRoutines::TryOpenFile( const string& fileName )
{
	try
	{
		m_pIn.open( fileName.c_str(), ios::in );
		//m_pIn->open( m_strFileName );
		m_pIn.exceptions( 
			ifstream::eofbit | ifstream::failbit | ifstream::badbit );
		if( !m_pIn && !m_pIn.is_open() )
		{
			AfxMessageBox( _T( "File Is NOT Open!" ) );
			return false;
		}
	}
	catch( exception const& e )
	{
		string strFormatted = "The file " + fileName + 
		   " could not be opened because of this error: ";
		strFormatted += e.what();
		AfxMessageBox( CString( strFormatted.c_str() ) );
		m_pIn.close();
		return false;
	}
	return true;
}


void CFileRoutines::ReadStat( vector< UINT >& stat )
{
	string line;	

	FindBlockPos( "STAT#" );
	
	getline ( m_pIn, line );

	string token;
	istringstream iss( line );
	while ( getline( iss, token, ' ') )
	{
		if( IsNumber( token ) )
			stat.push_back( stoi( token ) );
	}
}


void CFileRoutines::ReadVars( std::map< UINT, std::string >& vars )
{
	string line;
	FindBlockPos( "VAR#" );

	while( getline( m_pIn, line ) && ( line.find( "GRP#" ) == string::npos ) )
	{
		if( line.find( "VAR#" ) == string::npos )
			continue;

		UINT u;
		string name, s;
		istringstream iss( line );

		while( iss >> s )
		{
			if( s.find( ':' ) != string::npos ) 
			{
				s.erase( s.size() - 1 );
				u = stoi( s );
			}
//			Could be used if necessary
//			/*else if( s.front() == '(' )
//			{
//				if( s.find( "(i_" ) ) varInfo.byType = 'i';
//				else if( s.find( "(_o" ) ) varInfo.byType = 'o';
//				else varInfo.byType = 'm';
//			}*/
			else if( s.front() == '"' )
			{
				s.erase( 0, 1 );
				s.pop_back();
				name = s;
			}
		}
		vars.insert( std::pair< UINT, string >( u, name ) );
	}
	m_pIn.seekg( m_pIn.beg );//It is faster here to return carriage
}
	

void CFileRoutines::ReadGraphParam( CFileRoutines::GRAPHSTAT& stat )
{
	string line;	
	FindBlockPos( "GRP#" );

	getline( m_pIn, line );

	istringstream iss( line );
	string s;

	while ( iss >> s )
	{	
		if( s.back() == ':'  ) 
		{
			s.pop_back();
			stat.uGrpNum = stoi( s );
		}
		else if( s.back() == ',' )
		{
			s.pop_back();
			stat.uBegin = stoi( s );
		}
		else if( IsNumber( s ) )
		{
			stat.uLen = stoi( s );
			break;
		}
	}  //It is faster here to return carriage
}


void CFileRoutines::ReadGraph( vector< CFileRoutines::GRAPHDATA >& grp )
{
	string line;	
	FindBlockPos( "GRP#" );
	getline( m_pIn, line );

	while( !m_pIn.eof() && getline( m_pIn, line ) )
	{
		if( !( line.find( "VAR#" ) == string::npos ) || line.empty() )
			break;

		istringstream iss( line );
		vector< string > str;
		string s;

		while ( iss >> s )
			str.push_back( s );

		grp.push_back( FormatGraphData( str ) );
	}
}


const CFileRoutines::GRAPHDATA CFileRoutines::FormatGraphData( vector< string >& s )
{
	CFileRoutines::GRAPHDATA data;
	//here use pop_back when possible
	data.uLineNum = stoi( s.at( 0 ) );
	data.uOrderNum = stoi( s.at( 1 ).erase( s.at( 1 ).length() - 1 ) );
	data.iInverted = ( s.at( 2 ).at( 1 ) == 'I' ) ? 1 : 0;
	data.uDown = stoi( s.at( 4 ) );
	data.uRight = stoi( s.at( 5 ).erase( s.at( 5 ).length() - 1 ) );
	data.uVarNum = stoi( s.at( 8 ) );
	//data.strVarName = s.at( 9 ).erase( 0, 1 ).erase( s.at( 9 ).length() - 1 );

	return data;
}


void CFileRoutines::ReadVecCount( UINT& cnt )
{
	string line;	
	
	FindBlockPos( ".VECTORS" );
	
	getline ( m_pIn, line );

	string s;
	istringstream iss( line );

	while ( iss >> s )
	{
		if( IsNumber( s ) )
			cnt = stoi( s );
	}
}


void CFileRoutines::ReadVectors( vector< string >& vec, const string& type )
{
	string line;	
	
	FindBlockPos( type );
	
	getline ( m_pIn, line );
	getline ( m_pIn, line );

	while ( getline ( m_pIn, line ) )
	{
		if( line.empty() )
			break;
		vec.push_back( line );
	}
}


void CFileRoutines::ReadFaultString( string& s )
{
	string line;

	FindBlockPos( ".FAULTS" );

	getline ( m_pIn, line );
	getline ( m_pIn, line );
	getline ( m_pIn, line );
	
	s = line;
	
}


void CFileRoutines::FindBlockPos( const string& s )
{//siia lisada kontroll kui seda teksti ei leitud
	string line;
	LONGLONG curPos = 0;

	m_pIn.seekg( curPos );

	while( getline( m_pIn, line ) )
	{
		if( line.find( s ) != string::npos )
		{
			m_pIn.seekg( curPos );
			break;
		}
		curPos = m_pIn.tellg();
	}
	//throw exception
}

const bool CFileRoutines::IsNumber( const string& s ) const
{
    return !s.empty() && find_if( s.begin(), s.end(), 
		[]( char c ) { return !isdigit( c ); } ) == s.end();
}


bool IsMultiGraph( void )
{
	return TRUE;
}


//HRESULT IsNumber ( TCHAR *lpszInput, DWORD dwFlags )
//{
//   int        iSize       = lstrlen(lpszInput)+1;
//   OLECHAR    *lpwszInput = new OLECHAR[iSize*2];
//   UCHAR      *rgbDig     = new UCHAR[iSize];
//   NUMPARSE   numprs;
//   HRESULT    hRes;
//
//
//#ifdef _UNICODE
//   lstrcpy(lpwszInput, lpszInput);;
//#else
//   MultiByteToWideChar( CP_ACP, MB_PRECOMPOSED, lpszInput, iSize, lpwszInput, iSize );
//#endif
//
//   numprs.cDig = iSize;
//   numprs.dwInFlags = dwFlags;
//
//   hRes = VarParseNumFromStr(lpwszInput, GetUserDefaultLCID(), dwFlags, &numprs, rgbDig);
//
//   delete [] lpwszInput;
//   delete [] rgbDig;
//   return hRes;
//}

