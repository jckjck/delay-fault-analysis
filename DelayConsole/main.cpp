#include "Windows.h"
#include "CVector.h"
#include "Circuit.h"

#include <ctime>

#include <string>


using std::cout;
using std::endl;
using std::string;


int main( int argc, char** argv )
{
	clock_t t1 = GetTickCount();
	CCircuit cc;

	string test, graph;
	test = graph = argv[ 1 ];
	test += ".tst";
	graph += ".agm";
	cc.DelayVectorsJob( test, graph );
	
	clock_t t2 = GetTickCount();
	clock_t time = t2 - t1;
	cout <<  "OK" << endl;

	cout <<  "Process took: " << time << " ms" << endl;	

	// Print out separate times
	for( UINT u = 0; u < cc.GetTimersCount(); ++u )
		cc.GetTimeInfo( u ).PrintResult();


	// applyToolChain();
}