#include "stdafx.h"
#include "VectorTest.h"
#include <cstdlib> 
#include <ctime>

#include <string>
using std::string;

#include <bitset>
using std::bitset;

#include <vector>
using std::vector;





CVectorTest* CVectorTest::pInst( nullptr );


CVectorTest* CVectorTest::SingletonInst()
{
	if( !pInst )
		pInst = new CVectorTest();

	return pInst;
}


const ULONG CVectorTest::GenerateRandomBits( void )
{
	srand( ( ULONG )time( NULL ) );

	return static_cast< ULONG >( ULONG_MAX * ( rand() / double( RAND_MAX ) ) );
}


vector< bitset< 8 > >& CVectorTest::Generate8BitVectors( const int count )
{
	m_vec8bit.clear();
	srand( ( ULONG )time( NULL ) );

	for( int i = 0; i < count; ++i )
	{
		bitset< 8 > b( static_cast< ULONG >( ULONG_MAX * 
			( rand() / double( RAND_MAX ) ) ) );

		m_vec8bit.push_back( b );
	}

	return m_vec8bit;
}


vector< bitset< 16 > >& CVectorTest::Generate16BitVectors( const int count )
{
	m_vec16bit.clear();
	srand( ( ULONG )time( NULL ) );

	for( int i = 0; i < count; ++i )
	{
		bitset< 16 > b( static_cast< ULONG >( ULONG_MAX * 
			( rand() / double( RAND_MAX ) ) ) );

		m_vec16bit.push_back( b );
	}

	return m_vec16bit;
}


vector< bitset< 32 > >& CVectorTest::Generate32BitVectors( const int count )
{
	m_vec32bit.clear();
	srand( ( ULONG )time( NULL ) );

	for( int i = 0; i < count; ++i )
	{
		bitset< 32 > b( static_cast< ULONG >( ULONG_MAX * 
			( rand() / double( RAND_MAX ) ) ) );

		m_vec32bit.push_back( b );
	}

	return m_vec32bit;
}


vector< bitset< 64 > >& CVectorTest::Generate64BitVectors( const int count )
{
	m_vec64bit.clear();
	long twice =  2 * count, i = 0;
	bitset< 64 > b;
	srand( ( ULONG )time( NULL ) );

	while( 1 )
	{		
		bitset< 64 > temp( static_cast< ULONG >( ULONG_MAX * 
			( rand() / double( RAND_MAX ) ) ) );

		if( !( i % 2 ) && i )
		{
			m_vec64bit.push_back( b );
			b.reset();
			if( i == twice ) break;
		}
		if( i % 2 )
			temp <<= 32;

		b |= temp;
		++i;
	}
	return m_vec64bit;
}


vector< bitset< 128 > >& CVectorTest::Generate128BitVectors( const int count )
{
	m_vec128bit.clear();
	long fourTimes = count * 4, i = 0;
	bitset< 128 > b;
	srand( ( ULONG )time( NULL ) );

	while( 1 )
	{		
		bitset< 128 > temp( static_cast< ULONG >( ULONG_MAX * 
			( rand() / double( RAND_MAX ) ) ) );

		if( !( i % 4 ) && i )
		{
			m_vec128bit.push_back( b );
			b.reset();
			if( i == fourTimes ) break;
		}

		if( i % 4 )
			b <<= 32;

		b |= temp;
		++i;
	}
	return m_vec128bit;
}





CVectorTest::CVectorTest( void )
{
}


CVectorTest::~CVectorTest(void)
{
}
