#pragma once

#include <vector>
using std::vector;

#include <array>
using std::array;

class CFaultTable
{

public:
	CFaultTable( void );
	CFaultTable( UINT vars ) : m_uVarCount( vars ){};
	~CFaultTable(void);

	private:
	UINT m_uVarCount;
	vector< vector< UINT > > m_vecFaults;

};

