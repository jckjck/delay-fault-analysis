#include "Windows.h"
#include "CVector.h"


using std::vector;
using std::string;
using std::unique_ptr;
using std::shared_ptr;
using std::map;
using std::set;
using std::pair;
using std::for_each;
using std::cout;
using std::swap;
using std::endl;
using std::move;
using std::equal_to;
using std::make_pair;
using std::to_string;
using std::accumulate;


UINT CVector::GeneratePairSet( map< UINT, vector< UINT > >& mp, pairT& pairs, 
							  vector< CGraph >& graphs )
{
	UINT sz = GetInCnt();
	UINT count = 0;
	++m_curIndex;
	vector< string > secondary;
	// TODO: k]ik teha vektorite peale, mitte kasutada stringe
	m_Outs = m_Patterns[ m_curIndex ].substr( m_Length - m_Outputs );
	// Start finding pairs for current main vector 
	for( UINT v = 0; v < sz; ++v )
	{
		string res = m_Patterns[ m_curIndex ];
		auto key = mp.find( v );
		for( auto it = key->second.begin(); it != key->second.end(); ++it )
		{
			if( m_Table[ m_curIndex ][ *it ] != 'X' /*&& m_Cur[ v ] != 'x'*/ )
			{
				res[ v ] = ( m_Patterns[ m_curIndex ][ v ] == '1' ) ? '0' : '1';
				SimulateAll( res, graphs );
				// Use accumulate!!
				if( res.substr( m_Length - m_Outputs ).compare( m_Outs ) )
				{
					secondary.push_back( res );
					count += 2;
				}
				break;
			}
		}		
	}
	if( secondary.size() > 0 )
		pairs.insert( make_pair( m_Patterns[ m_curIndex ], secondary ) );

	return count;
}


void CVector::SimulateAll( string& prStr, vector< CGraph >& graphs )
{
	for( auto i = graphs.begin(); i != graphs.end(); ++i )
	{
		i->SetNodeValues( 0, prStr );
		// Convert FF simul result value (1 or 0) to char
		prStr[ i->GetEndNode() ] = ( char )( ( i->FaultFreeSimul( i->m_Graph ) + int( '0' ) ) );
	}
}


void CVector::MergeVectors( const pairT& pairs, vector< string >& merged, const UINT& looseBits,
				  vector< CGraph >& graphs )
{
	for( auto it : pairs )
	{
		if( it.second.size() < looseBits )
			continue;
		UINT size = it.second.size();
		UINT pos = 0;
		UINT next = pos + 1;
		while( size >= looseBits )
		{
			string cur = it.second[ pos ];
			for( UINT u = pos; u < looseBits; ++u )
			{
				UINT count = 0;
				string tmp = it.second[ next ];
				for( UINT u = 0; u < cur.size(); ++u )
				{
					if( cur[ u ] != tmp[ u ] )
						++count;
					if( count == looseBits )
					{
						cur[ u ] = tmp[ u ];
						break;
					}
				}
				++next;
				SimulateAll( cur, graphs );
				merged.push_back( it.first );
				merged.push_back( cur );
			}
			pos += looseBits;
			size -= looseBits;
		}
	}
}


void CVector::GeneratePairAnalysis( const string& source, const string& result,
								   map< UINT, vector< UINT > >& fanouts, vector< Timer >& timers )
{
	cout << "SAF test count: " << m_Patterns.size() << endl;
	// Create CDataExtractor objects for input and output
	dxIn.reset( new CDataExtractor( source ) ); 
	dxOut.reset( new CDataExtractor( result ) );
	// Check file extension
	if( !CDataExtractor::IsGoodExt( source, "tst" ) ) // TODO: go for try-catch EVERYWHERE
		cout << "Wrong extension in source file" << endl;
	// If extension is good try open files
	if( !dxIn->TryOpenInStream() || !dxOut->TryOpenOutStream( result ) )
		cout << "Error opening files" << endl;
	// Process pairset
	cout << "Starting to process pair inputs" << endl;
	m_Patterns.clear();
		// m_Table.clear();
	elemOuts.clear();
	// Read all vector pairs and fault string pairs
	dxIn->ReadVectors( m_Patterns, ".PATTERNS", m_Outputs );
	dxIn->ReadVectors( m_Table, ".TABLE", true );
	dxIn->ReadVectors( elemOuts, ".GRAPH_FAULTS", true );
	// First test round
	ProcessPairInputs( fanouts, timers );
}


void CVector::FlipSecodVectorBits( const string& source )
{
	// Create CDataExtractor objects for input and output
	CDataExtractor* dxi = new CDataExtractor( source );
	// Check file extension
	if( !CDataExtractor::IsGoodExt( source, "tst" ) ) // TODO: go for try-catch EVERYWHERE
		cout << "Wrong extension in source file" << endl;
	// If extension is good try open files
	if( !dxi->TryOpenInStream() /*|| !dxo->TryOpenOutStream( source )*/ )
		cout << "Error opening files" << endl;
	// Process pairset
	dxi->ReadVectors( m_Patterns, ".PATTERNS" );
	dxi->ReadVectors( m_Table, ".TABLE" );
	// Get and check sizes
	const UINT testSize = m_Patterns.size();
	const UINT vecSize = m_Patterns[ 0 ].size();
	static_assert( sizeof( m_Patterns ) == sizeof( m_Table ), 
		"Sizes are not equal for test and fault tables!\n" );
	// Loop through all vector pairs and flip bits, if needed
	for( UINT u = 0; u < testSize; u += 2 )
	{
		for( UINT v = 0; v < vecSize; ++v )
		{
			if( Helper::IsSAFTested( m_Table[ u ][ v ] ) )
				m_Patterns[ u + 1 ][ v ] = Helper::IsHigh( m_Patterns[ u + 1 ][ v ] ) ? '0' : '1';
		}
	}
	delete dxi;
	std::ofstream os( source, std::ios::out );
	os.seekp( 0, std::ios::beg );
	os << "\n.VECTORS " << testSize << "\n\n\n.PATTERNS\n\n";
	for( auto& it : m_Patterns )
		os << it << endl;
	os << "\n\n";
	os.close();
}


void CVector::ProcessInitialTests( vector< CDataExtractor::TIMERINFO >& timers )
{
	// TODO: Init timer!

	// Get vector sizes and verify that they are equal
	const UINT testSize = m_Patterns.size();

	static_assert( sizeof( m_Patterns ) == sizeof( m_Table ), 
		"Sizes are not equal for test and fault tables!\n" );

	for( UINT u = 0; u < testSize; ++u )
	{		
		for( UINT v = u + 1; v < testSize && elements.size(); ++v )
		{
			// RunElementTest( fanouts, u );
		}
	}
}


void CVector::ProcessPairInputs( map< UINT, vector< UINT > >& fanouts, vector< Timer >& timers )
{
	// Wright leading data to output
	//dxOut->WriteLine( "\n.PAIRS\n\n" );
	dxOut->WriteLine( "Robust;Functional;NonRobust;NonRobust-Functional;Overall Cover;Tests Run" );
	cover.assign( m_Table[ 0 ].length(), '_' );

	UINT u = 0;
	cout << "Graph count: " << elements.size() << endl;
	Timer timer( "TDF analysis" );
	// Start timer
	timer.StartCounter();
	
	GetFaultsByElement( fanouts );
	
	timer.StopCounter();

	timers.push_back( timer );
}


void CVector::GetFaultsByElement( map< UINT, vector< UINT > >& fanouts )
{
	finalCount = 0;
	UINT tsize = m_Patterns.size();
	cout << "Initial Pair Count: " << tsize << endl;
	// Set controlpoints count
	UINT measurePoint = tsize / 48;
	for( pairCount = 0; pairCount < tsize; ++pairCount )
	{
		if( elements.empty() )
		{
			cout << "All tested" << endl;
			break;
		}		
		if( pairCount % measurePoint == 0 )
		{
			//PrepareCover();
			PrintCoverCSV();
		}
		RunElementTest( fanouts, pairCount );
	}
	PrintCoverCSV();
	cout << "Selected Pair Count: " << finalCount << endl;
}


void CVector::PrepareCover( void )
{
	for( auto it = elements.begin(); it != elements.end(); ++it )
	{
		for( UINT u = 0; u < it->get()->GetInputSIze(); ++u )
		{
			char val = it->get()->GetTestInfo( u );
			UINT place = it->get()->GetInput( u );
			InsertToCover( val, place );
		}
	}

	for( auto it = tested.begin(); it != tested.end(); ++it )
	{
		for( UINT u = 0; u < it->get()->GetInputSIze(); ++u )
		{
			char val = it->get()->GetTestInfo( u );
			UINT place = it->get()->GetInput( u );
			InsertToCover( val, place );
		}
	}
}


void CVector::InsertToCover( const char& ch, const UINT& pos )
{
	switch( ch )
	{
		case 'X':
			if( cover[ pos ] == '_' )
			{
				cover[ pos ] = 'X';
				cout << "X discovered +++++++++++++++++++\n";
			}			
			break;
		case 'N':
			if( cover[ pos ] == '_' || cover[ pos ] == 'X' )
				cover[ pos ] = 'N';
			break;
		case 'F':
			if( cover[ pos ] == '_' || cover[ pos ] == 'X'
				|| cover[ pos ] == 'N' )
				cover[ pos ] = 'F';
			break;
		case 'R':
			cover[ pos ] = 'R';
			break;
		default:
			break;
	}
}


void CVector::RunElementTest( map< UINT, vector< UINT > >& fanouts, const UINT num )
{
	static UINT counter = 0;
	UINT selected = 0;
	static UINT sum = 0;
	UINT tmp = 0;

	for( int v = ( elements.size() - 1 ); v >= 0; )
	{
		if( ( Helper::IsSAFTested( elemOuts[ num ][ elements[ v ]->GetGraphNum() ] ) 
				|| Helper::IsDelayTested( cover[ elements[ v ]->GetGraphNum() ] )) 
			&& (elements[ v ]->TestElement( m_Patterns[ num ], cover, fanouts )) )
		{
			swap( elements[ v ], elements.back() );
			// tested.push_back( move( elements[ elements.size() - 1 ] ) );
			elements.pop_back();
		}
		--v;
	}
	// If needed these vectors can be modified and print out as a test packet (selected)
	if( ( tmp = std::accumulate( cover.begin(), cover.end(), 0 ) ) != sum )
	{
		++finalCount;
		sum = tmp;
	}
}


void CVector::PrintCover( void )
{
	dxOut->WriteLine( "\n.FAULTS\n" );
	dxOut->WriteLine( cover );
	// Variables for discoverable errors and functional errors
	UINT Rerr = 0, Ferr = 0, Nerr = 0, Xerr = 0;
	// Get the count of all errors
	UINT sz = cover.length();
	// Count delay errors found
	for( UINT u = 0; u < sz; ++u )
	{
		if( cover[ u ] == 'R' || cover[ u ] == 'r' )
			++Rerr;
		else if( cover[ u ] == 'F' )
			++Ferr;
		else if( cover[ u ] == 'N' )
			++Nerr;
		else if( cover[ u ] == 'X' )
			++Xerr;
	}
	// Calc coverage procent for both delay aults discovered
	double Rcoverage = ( static_cast< double >( Rerr ) / sz ) * 100;
	double Fcoverage = ( static_cast< double >( Ferr ) / sz ) * 100;
	double Ncoverage = ( static_cast< double >( Nerr ) / sz ) * 100;
	double Xcoverage = ( static_cast< double >( Xerr ) / sz ) * 100;
	// Print out information 
	dxOut->WriteLine( "\n.COVERAGE\n" );
	// Form string representation of cover
	dxOut->GetOutStream() << "Robust dealy faults: " << Rerr << " / " 
		<< sz << " = " << Rcoverage << "%" << endl;
	dxOut->GetOutStream() << "Functional delay faults: " << Ferr << " / " 
		<< sz << " = " << Fcoverage << "%" << endl;
	dxOut->GetOutStream() << "Non-Robust delay faults: " << Nerr << " / " 
		<< sz << " = " << Ncoverage << "%" << endl;
	dxOut->GetOutStream() << "Non-Robust Functional delay faults: " << Xerr << " / " 
		<< sz << " = " << Xcoverage << "%" << endl;
	dxOut->GetOutStream() << "Overall coverage: " <<  Rcoverage + Ncoverage  << "%" << endl;
	// Print out number of pairs
	dxOut->WriteLine( "\n.COUNT\n" );
	//dxOut->GetOutStream() << pairCount << " pairs analysed" << endl;
	dxOut->GetOutStream() << finalCount << " pairs selected" << endl;
}


void CVector::PrintCoverCSV( void )
{
	UINT Rerr = 0, Ferr = 0, Nerr = 0, Xerr = 0;
	// Get the count of all errors
	UINT sz = cover.length();
	// Count delay errors found
	for( UINT u = 0; u < sz; ++u )
	{
		if( cover[ u ] == 'R' || cover[ u ] == 'r' )
			++Rerr;
		else if( cover[ u ] == 'F' )
			++Ferr;
		else if( cover[ u ] == 'N' )
			++Nerr;
		else if( cover[ u ] == 'X' )
			++Xerr;
	}
	// Calc coverage procent for both delay aults discovered
	double Rcoverage = ( static_cast< double >( Rerr ) / sz ) * 100;
	double Fcoverage = ( static_cast< double >( Ferr ) / sz ) * 100;
	double Ncoverage = ( static_cast< double >( Nerr ) / sz ) * 100;
	double Xcoverage = ( static_cast< double >( Xerr ) / sz ) * 100;
	
	string coverString = "";
	// Form string representation of cover
	dxOut->GetOutStream() << Helper::DoubleToCSV( Rcoverage, true );
	dxOut->GetOutStream() << Helper::DoubleToCSV( Fcoverage, true );
	dxOut->GetOutStream() << Helper::DoubleToCSV( Ncoverage, true );
	dxOut->GetOutStream() << Helper::DoubleToCSV( Xcoverage, true );
	dxOut->GetOutStream() << Helper::DoubleToCSV( Rcoverage + Ncoverage + Fcoverage + Xcoverage, true );
	dxOut->GetOutStream() << finalCount;
	dxOut->GetOutStream() << endl;
}