#pragma once

//CUSTOM HEADERS
#include "CVector.h"
#include "CGraph.h"
#include "CDataExtractor.h"
#include "ElementTestInfo.h"
#include "Timer.h"

#include "Windows.h"

//STL HEADERS
#include <memory>
#include <tuple>
#include <string>
#include <vector>
#include <map>
#include <set>

typedef unsigned int UINT;
typedef std::map< std::string, std::vector< std::string > > pairT;
// Table size constant
const UINT rowHeaderWidth = 30;


class CCircuit
{

private:

/* VARIABLES */

	// PRIMITIVES

	UINT m_uNodCount;
	UINT m_uVarCount;
	UINT m_uGrpCount;
	UINT m_uInCount;
	UINT m_uConCount;
	UINT m_uOutCount;
	UINT m_uVecCount;	


	// CONTAINERS

	std::vector< CGraph > m_Graphs;//without pointers would be faster
	pairT pairs;
	std::vector< std::string> mergedPairs;
	std::vector< Timer > timers;
	
	std::map< UINT, std::vector< UINT > > m_Var2Nod;
	std::map< UINT, std::vector< UINT > > fanouts;
	//std::map< UINT, std::set< UINT > > m_Grp2Var;
	// File names
	std::string origAgm;
	std::string newAgm;
	std::string callString;
	std::string callString2;
	std::string callString3;
	std::string newTst;
	std::string pairsDsv;
	std::string tableFile;

	// MEMBER OBJECTS

	std::unique_ptr< CDataExtractor > m_DXgrp;	
	std::unique_ptr< CVector >		  m_CVect;
	std::shared_ptr< CDataExtractor > m_DXvec;

	

/* METHODS */

	void SetGraphs( const bool needStack );
	void SetGraphStats( void );
	void SetVectors( void );
	void GeneratePairs( void );
	void GenereatePairsFromSAFTest( void );

	const bool OpenFiles( const std::string& v, const std::string& g );
	
	// Create new testfile name
	const std::string MakeNewFileName( const std::string& oldName, 
		const std::string& ext, const bool addExt );

	// Prepare all strings, including file names and callstrings
	void PrepareStrings( const std::string& tst, const std::string& agm );

	// Function for creating SHELLEXECUTEINFO struct for Devadze tool call
	void ShellDevadze( LPCSTR param, SHELLEXECUTEINFO* ShExecInfo );

	// Function for handling SHELLEXECUTEINFO struct pointer and calling ShellExecuteEx
	const bool CallDevadzeTool( LPCSTR param );

	// Fill TABLEDATA structure with parameters for table
	CDataExtractor::TABLEDATA& FillTableData( CDataExtractor::TABLEDATA& dat );

	// Generate testinfo for all the graphs, based on m_Graphs
	void GenerateTestInfo( std::vector< std::unique_ptr< CElementTestInfo > >& testData );

	// List all fanouts for
	void GenerateFanoutMap( void );



public:

/* VARIABLES */

	// CONTAINERS

/* METHODS */

	// Construct/Destruct
	CCircuit( void ); 
	~CCircuit( void );	

	// Getters
	const UINT GetNodCount( void ) { return m_uNodCount; };
	const UINT GetVarCount( void ) { return m_uVarCount; };
	const UINT GetGrpCount( void ) { return m_uGrpCount; };
	const UINT GetInCount ( void ) { return m_uInCount;  };
	const UINT GetConCount( void ) { return m_uConCount; };
	const UINT GetOutCount( void ) { return m_uOutCount; };
	const UINT GetVecCount( void ) { return m_uVecCount; };

	//inline const UINT GetNodForVar( const UINT var ) const
	//	{	return m_Var2Nod.at( var );	};

	// Setters
	void SetNodCount( UINT nods ) { m_uNodCount = nods; };
	void SetVarCount( UINT vars ) { m_uVarCount = vars; };
	void SetGrpCount( UINT grps ) { m_uGrpCount = grps; };
	void SetInCount ( UINT ins  ) { m_uInCount  = ins;  };
	void SetConCount( UINT cons ) { m_uConCount = cons; };
	void SetOutCount( UINT outs ) { m_uOutCount = outs; };
	void SetVecCount( UINT vecs ) { m_uVecCount = vecs; };

	// Custom

	// Toolchain for generating delay tests and their analysis
	const bool DelayVectorsJob( const std::string& vects, const std::string& grp );

	// Get TIMEINFO struct from desired place in vector
	inline Timer& GetTimeInfo( const UINT pos ) { return timers.at( pos ); };

	// Get count of all timers used
	inline const UINT GetTimersCount( void ) const { return timers.size(); };

};


