#include "Timer.h"

using std::cout;
using std::endl;
using std::string;



void Timer::StartCounter( void )
{
	LARGE_INTEGER li;
	// Try __stdcall
	if( !QueryPerformanceFrequency( &li ) )
		cout << "QueryPerformanceFrequency failed!" << endl;

	PCFreq = double( li.QuadPart ) / 1000.0;

	QueryPerformanceCounter( &li );
	counterStart = li.QuadPart;
	// Marked timer as started
	started = true;
}


void Timer::StopCounter( void )
{
	LARGE_INTEGER li;

	if( !started )
	{
		cout << "Timer wass not started, exiting function..." << endl;
		return;
	}

	QueryPerformanceCounter(&li);
	counterStop = li.QuadPart;
}


void Timer::PrintResult( void )
{
	// Get duration
	double result = double( counterStop - counterStart ) / PCFreq;

	//Print it out
	cout << timerName << ": " << result << " ms" << endl;
}


Timer::Timer( const string& name )
{
	PCFreq = 0.0;
	counterStart = 0;
	counterStop = 0;
	timerName = name;
}


Timer::~Timer( void )
{
}
