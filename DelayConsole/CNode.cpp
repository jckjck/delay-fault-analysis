// CNode.cpp : implementation file
// Created by Jaak K�usaar @08.2013

#include "CNode.h"

using std::string;

// CNode


CNode::CNode( void )
{
	m_uFaultVal = 0;
	m_uNodeNum = 0;
	m_Val = 0;
	m_uVarNum = 0;
	m_isInverted = false;
	m_pDown = 0;
	m_pRight = 0;
}

CNode::CNode( UINT node, UINT var )
{
	m_uFaultVal = 0;
	m_uNodeNum = node;
	m_Val = 0; //To Be Tested!
	m_uVarNum = var;
	m_isInverted = false;
	m_pDown = 0;
	m_pRight = 0;
}

CNode::CNode( const CNode& orig )
{
	m_uFaultVal = orig.m_uFaultVal;
	m_uNodeNum = orig.m_uNodeNum;
	m_uVarNum = orig.m_uVarNum;
	m_Val = orig.m_Val; 
	m_isInverted = orig.IsInverted();
	m_pDown = orig.m_pDown;
	m_pRight = orig.m_pRight;
}

CNode& CNode::operator= ( const CNode& c )
{
    if ( this == &c )
        return *this;
 
	m_uFaultVal = c.m_uFaultVal;
	m_uNodeNum = c.m_uNodeNum;
	m_uVarNum = c.m_uVarNum;
	m_Val = c.m_Val; 
	m_isInverted = c.IsInverted();
	m_pDown = c.m_pDown;
	m_pRight = c.m_pRight;

    return *this;
}

CNode::CNode( CNode&& orig )
{
	m_uFaultVal = orig.m_uFaultVal;
	m_uNodeNum = orig.m_uNodeNum;
	m_uVarNum = orig.m_uVarNum;
	m_Val = orig.m_Val; 
	m_isInverted = orig.m_isInverted;
	m_pDown = orig.m_pDown;
	m_pRight = orig.m_pRight;
}

CNode& CNode::operator= ( CNode&& orig )
{
	if( this != &orig )
	{
		m_uFaultVal = orig.m_uFaultVal;
		m_uNodeNum = orig.m_uNodeNum;
		m_uVarNum = orig.m_uVarNum;
		m_Val = orig.m_Val; 
		m_isInverted = orig.m_isInverted;
		m_pDown = orig.m_pDown;
		m_pRight = orig.m_pRight;
	}
	return *this;
}

CNode::~CNode()
{
}






