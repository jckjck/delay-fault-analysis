
// DelayDlg.h : header file
//

#pragma once
#include "afxwin.h"


// CDelayDlg dialog
class CDelayDlg : public CDialogEx
{
// Construction
public:
	CDelayDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_DELAY_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void OnBnClickedOk();

public:
	CEdit m_edtFileName;
	CEdit m_edtExtension;
	//CStatic m_lblResult;
	CListBox m_lstData;
	afx_msg void OnBnClickedOk();
	void applyToolChain(void);
};
