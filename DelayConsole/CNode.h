#pragma once

// STL HEADERS
#include <string>
#include <memory>
// CNode

#define NOD_INV 0x00000001
#define NOD_ONE 0x00000010
#define NOD_ZER 0x00000100

typedef unsigned int UINT;

class CNode
{
public://must be private
	UINT m_uFaultVal;
	UINT m_uNodeNum;
	UINT m_Val;
	bool m_isInverted;
	UINT m_uVarNum;
	UINT m_pRight;
	UINT m_pDown;
public:
	//CONSTRUCTORS/DESTRUCTOR
	CNode( UINT node, UINT var );
	CNode( void );
	// Copy constructor and copy assignment operator
	CNode( const CNode& orig );
	CNode& operator= ( const CNode& c );
	// Move constructor and move assignment operator
	CNode( CNode&& orig );
	CNode& operator= ( CNode&& orig );
	~CNode();

	//STATIC METHODS
	static const bool SameVal( const CNode* a, const CNode* b )
	{
		return ( a->IsOne() == b->IsOne() );
	};
	static const bool BothNotInv( const CNode* a, const CNode* b )
	{
		return ( !a->IsInverted() && !b->IsInverted() );
	};
	static const bool DifInvVal( const CNode* a, const CNode* b )
	{
		return ( a->IsInverted() != b->IsInverted() );
	};

	//PUBLIC Methods
	inline void CNode::SetFaultVal( UINT val )
	{
		m_uFaultVal = val;
	}

	inline void CNode::SetNodeNum( UINT num )
	{
		m_uNodeNum = num;
	};

	inline void CNode::SetVarNum( UINT var )
	{
		m_uVarNum = var;
	};

	inline void CNode::ToggleInversion( void )
	{
		m_isInverted = ( m_isInverted ? false : true );
	};

	inline void CNode::ToggleOne( void )
	{
		m_Val = 1;
	};

	inline void CNode::ToggleZero( void )
	{
		m_Val = 0;
	};

	inline void CNode::SetRChild( UINT u )
	{
		m_pRight = u;
	};

	inline void CNode::SetDChild( UINT u )
	{
		m_pDown = u;
	};

	inline const bool CNode::IsOne( void ) const
	{
		return ( m_Val == 1 );
	};

	inline const bool CNode::IsInverted( void ) const
	{
		return m_isInverted;
	};

	inline const UINT CNode::GetFaultVal( void ) const 
	{
		return m_uFaultVal;
	}

	inline const UINT CNode::GetNodeNum( void ) const
	{
		return m_uNodeNum;
	};

	inline const UINT CNode::GetVarNum( void ) const
	{
		return m_uVarNum;
	};

	inline UINT CNode::GetRChild( void ) const
	{
		return m_pRight;
	};

	inline UINT CNode::GetDChild( void ) const
	{
		return m_pDown;
	};
};


