#pragma once


#include <vector>
using std::vector;

#include <map>

#include <string>
using std::string;

#include <fstream>
using std::ifstream;

#include <stdexcept>
using std::exception;

#include <iostream>
using std::ios;
using std::cin;

#include <sstream>
using std::istringstream;

#include <algorithm> 


//correct style with namespaces
class CFileRoutines
{
public:

	struct VARINFO
	{
		byte byType;
		string sName;
		ULONG ulVarNum;
	};

	struct GRAPHSTAT
	{
		UINT uGrpNum;
		UINT uBegin;
		UINT uLen;
	};

	struct GRAPHDATA
	{
		UINT uLineNum;
		UINT uOrderNum;
		int iInverted;
		UINT uDown;
		UINT uRight;
		UINT uVarNum;
		string strVarName;
	};


private:

	ifstream m_pIn;
	CFileException m_exFile;
	string m_strFileName;
	//METHODS	
	const bool IsNumber( const string& s ) const;
	void FindBlockPos( const string& name );
	const CFileRoutines::GRAPHDATA FormatGraphData( vector< string >& s );
	

public:

	CFileRoutines( const string& fName ) : m_strFileName( fName )
	{
	};

	bool TryOpenFile( const string& fileName );
	bool IsMultiGraph( void );
	void ReadStat( vector< UINT >& stat );
	void ReadVars( std::map< UINT, std::string >& vars );
	void ReadGraph( vector< CFileRoutines::GRAPHDATA >& grp );
	void ReadGraphParam( CFileRoutines::GRAPHSTAT& stat );
	void ReadVecCount( UINT& cnt );
	void ReadVectors( vector< string >& vec, const string& type );
	void ReadFaultString( string& s );
	static bool IsGoodExt( const string& file, const string& ext );
	
	~CFileRoutines( void )
	{
		m_pIn.close();
	};
};

