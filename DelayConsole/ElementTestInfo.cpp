#include "ElementTestInfo.h"

using std::find;
using std::string;
using std::cout;
using std::endl;
using std::map;
using std::vector;


const bool CElementTestInfo::TestElement( const string& S1, string& cov,
										 map< UINT, vector< UINT > >& fanouts )
{
	codedInputs.clear();
	test2AND.clear();

	// Determine element class
	if( typeName == "\"GTECH_NAND2\"" || typeName == "\"GTECH_NAND4\"" )
		return TestAndClass( S1, cov, fanouts );
	else if( typeName == "\"GTECH_NOR2\"" || typeName == "\"GTECH_NOR4\"" )
		return TestOrClass( S1, cov, fanouts );
	else if( typeName == "\"GTECH_AOI22\"" )
		return TestAOIClass( S1, cov, fanouts );
	else if( typeName == "\"GTECH_NOT\"" )
		return TestNOT( S1, cov, fanouts );
	else if( typeName == "\"\"" ) 
		return TestOUT( S1, cov );
	else
	{
		cout << "Unknown element..." << endl;
		return false;
	}
}

const bool CElementTestInfo::TestNOT( const string& S1, string& cov,
									 map< UINT, vector< UINT > >& fanouts )
{
	auto key = fanouts.find( outPut );
	return ( cov[ faultNumbers[ 0 ] ] = cov[ key->second[ 0 ] ] ) == 'R' ? true : false;
}


const bool CElementTestInfo::TestOUT( const string& S1, string& cov )
{
	if( S1[ testNumbers[ 0 ] ] == 'h' || S1[ testNumbers[ 0 ] ] == 'e' )
	{
		cov[ faultNumbers[ 0 ] ] = 'R';
		return true;
	}
	return false;
}


void CElementTestInfo::CombineElementInputs( string& S1, string& S2, string& res )
{
	for( auto tmp : testNumbers )
	{
		if( !Helper::IsHigh( S1[ tmp ] ) && !Helper::IsHigh( S2[ tmp ] ) )
			res[ tmp ] =  '0';
		else if( !Helper::IsHigh( S1[ tmp ] ) && Helper::IsHigh( S2[ tmp ] ) )
			res[ tmp ] =  'e';
		else if( Helper::IsHigh( S1[ tmp ] ) && !Helper::IsHigh( S2[ tmp ] ) )
			res[ tmp ] =  'h';
		else
			res[ tmp ] =  '1';
	}
}


const bool CElementTestInfo::IsOutputTested( const std::string& R2 ) const
{
	for( UINT u = 0; u < faultNumbers.size(); ++u )
	{
		char temp = R2[ faultNumbers[ u ] ];

		if( temp == '1' || temp == '0' )
			return true;
	}
	return false;
}


const bool CElementTestInfo::IsFunctionalOutput( const string& R1 ) const
{
	for( UINT u = 0; u < faultNumbers.size(); ++u )
	{
		char temp = R1[ faultNumbers[ u ] ] ;

		if( temp != '1' || temp != '0' )
			return false;
	}
	return true;
}


const char CElementTestInfo::GetInheritedValue( map< UINT, vector< UINT > >& fanouts, 
										 std::string& cov)
{
	char tmp = '_';
	char prev = '_';
	// Get current output variable from map
	auto key = fanouts.find( outPut );
	// Loop through mapped vector
	for( auto it = key->second.begin(); it != key->second.end(); ++it )
	{
		// If one of the branches are robust tested, then we have our result
		if( cov[ ( *it ) ] == 'R' )
		{
			tmp = 'R';
			break;
		}
		else if( cov[ ( *it ) ] == 'N' || cov[ ( *it ) ] =='F' )
			tmp = ( ( prev == 'N' || prev == 'F' ) && ( prev != tmp ) ) ? 'X' : cov[ ( *it ) ];
		else if( cov[ ( *it ) ] == 'X' && tmp == '_' )
			tmp = 'X';

		prev = tmp;
	}
	return tmp;
}


void CElementTestInfo::EvaluateOutputs( const UINT indx, const char& inter, const char& inh,
									    const string& S1, string& cov, const bool and )
{
	char temp = '_';
	char delay = and ? DetectANDFault( S1[ testNumbers[ indx ] ], inter ) :
		DetectORFault( S1[ testNumbers[ indx ] ], inter );
	if( delay =='0' ) 
		return;
	else if( delay == 'X' && cov[ faultNumbers[ indx ] ] == '_' )
		temp = 'X';
	else if( delay == 'N' && ( cov[ faultNumbers[ indx ] ] == '_' || cov[ faultNumbers[ indx ] ] == 'X' ) )
		temp = 'N';
	else if( delay == 'F' && ( cov[ faultNumbers[ indx ] ] == '_' || cov[ faultNumbers[ indx ] ] == 'X' ) )
		temp = 'F';
	else if( delay == 'R' )
		temp = 'R';
	else
		return;

	if( temp == 'X' && inh < temp )
		temp = inh;
	else if( ( temp == 'N' && inh == 'X' ) || ( temp == 'F' && inh == 'X' ) )
		temp = 'X';
	else if( temp == 'R' && ( inh < temp || temp == 'X' ) )
		temp = inh;
	else if( ( temp == 'F' && inh == 'N' ) || ( temp == 'N' && inh == 'F' ) )
		temp = 'X';
	else if( temp == 'R' && inh == 'R' )
		temp = 'R';

	testedInputs[ indx ] = cov[ faultNumbers[ indx ] ] = temp;
}


const bool CElementTestInfo::TestAndClass( const string& S1, string& cov,
										  map< UINT, vector< UINT > >& fanouts )
{
	UINT temp = testNumbers.size();
	char interMediate = S1[ testNumbers[ 0 ] ];
	// Loop through inputs and calculate fault type
	for( UINT v = 1; v < temp; ++v )
		interMediate = CalculateANDType( interMediate, S1[ testNumbers[ v ] ] );
	char inherited = GetInheritedValue( fanouts, cov );
	// Loop through inputs again and now detect fault
	for( UINT u = 0; u < temp; ++u )
	{
		if( testedInputs[ u ] == 'R' )
			continue;
		if( cov[ faultNumbers[ u ] ] == 'R' && inherited == 'R' )
		{
			testedInputs[ u ] = 'R';
			continue;
		}
		EvaluateOutputs( u, interMediate, inherited, S1, cov, true );
	}
	for( auto& tmp : testedInputs )
	{
		if( tmp != 'R' )
			return false;
	}
	return true;
}


const char CElementTestInfo::CalculateANDType( const char c1 /* interMediate */, const char c2 )
{
	char tmp = ' ';

	if( c1 == '0' || c2 == '0' )
		tmp = '0';
	else if( c2 == '1' )
		tmp = c1;
	else if( c1 == '1' )
		tmp = c2;
	else if( c1 == 'X' || c2 == 'X' )
		tmp = 'X';
	else if( c2 == 'e' )
	{
		if( c1 == 'e' )
			tmp = 'e';
		else if( c1 == 'h' || c1 == 'N' )
			tmp = 'N';
		else 
			tmp = 'X';
	}
	else if( c2 == 'h' )
	{
		if( c1 == 'e' )
			tmp = 'N';
		else if( c1 == 'h' || c1 == 'F' )
			tmp = 'F';
		else
			tmp = 'X';
	}
	return tmp;
}


const char CElementTestInfo::DetectANDFault( const char c1, const char c2 /* interMediate */)
{
	char tmp = ' ';
	if( c1 == '1' || c1 == '0' || c1 == 'X' )
		tmp = '0';
	else if( c2 == '0' )
		tmp = '0';
	else if( c1 == 'e' )
	{
		if( c2 == '1' || c2 == 'e' )
			tmp = 'R';
		else 
			tmp = '0';
	}
	else if( c1 == 'h' )
	{
		if( c2 == '1' )
			tmp = 'R';
		else if( c2 == 'e' || c2 == 'N' )
			tmp = 'N';
		else if( c2 == 'h' || c2 == 'F' )
			tmp = 'F';
		else
			tmp = 'X';
	}
	return tmp;
}


const bool CElementTestInfo::TestOrClass( const string& S1, string& cov, 
										 map< UINT, vector< UINT > >& fanouts )
{
	UINT temp = testNumbers.size();
	char interMediate = S1[ testNumbers[ 0 ] ];
	// Loop through inputs and calculate fault type
	for( UINT v = 1; v < temp; ++v )
		interMediate = CalculateORType( interMediate, S1[ testNumbers[ v ] ] );
	char inherited = GetInheritedValue( fanouts, cov );
	// Loop through inputs again and now detect fault
	for( UINT u = 0; u < temp; ++u )
	{
		if( testedInputs[ u ] == 'R' )
			continue;
		if( cov[ faultNumbers[ u ] ] == 'R' && inherited == 'R' )
		{
			testedInputs[ u ] = 'R';
			continue;
		}
		EvaluateOutputs( u, interMediate, inherited, S1, cov, false );
	}
	for( auto& tmp : testedInputs )
	{
		if( tmp != 'R' )
			return false;
	}
	return true;
}


const char CElementTestInfo::CalculateORType( const char c1 /*interMediate */, const char c2 )
{
	char tmp = ' ';

	if( c1 == '1' || c2 == '1' )
		tmp = '1';
	else if( c1 == '0' )
		tmp = c2;
	else if( c1 == 'X' || c2 == '0' )
		tmp = c1;
	else if( c2 == 'e' )
	{
		if( c1 == 'e' || c1 == 'F' )
			tmp = 'F';
		else if( c1 == 'h')
			tmp = 'N';
		else
			tmp = 'X';
	}
	else if( c2 == 'h' )
	{
		if( c1 == 'e'|| c1 == 'N' )
			tmp = 'N';
		else if( c1 == 'h' )
			tmp = 'h';
		else 
			tmp = 'X';
	}
	return tmp;
}


const char CElementTestInfo::DetectORFault( const char c1, const char c2 /*interMediate */ )
{
	char tmp = ' ';

	if( c1 == '1' || c1 == '0' || c1 == 'X' )
		tmp = '0';
	else if( c2 == '0' )
		tmp = 'R';
	else if( c2 == '1' )
		tmp = '1';
	else if( c1 == 'e' )
	{
		if( c2 == 'e' || c2 == 'F' )
			tmp = 'F';
		else if( c2 == 'h' || c2 == 'N' )
			tmp = 'N';
		else
			tmp = 'X';
	}
	else if( c1 == 'h' )
	{
		if( c2 == 'h' )
			tmp = 'R';
		else
			tmp = '0';
	}
	return tmp;
}


const bool CElementTestInfo::TestAOIClass( const string& S1, string& cov, 
										  map< UINT, vector< UINT > >& fanouts )
{
	char i0 = S1[ testNumbers[ 0 ] ];
	char i1 = S1[ testNumbers[ 1 ] ];
	char i2 = S1[ testNumbers[ 2 ] ];
	char i3 = S1[ testNumbers[ 3 ] ];
	UINT flag = 0;

	if( i0 == '0' || i1 == '0' || ( i0 == 'e' && i1 == 'h' ) || ( i0 == 'h' && i1 == 'e' ) )
	{
		codedInputs.push_back( i2 ); codedInputs.push_back( i3 );
		flag = 2;
	}
	else if( i2 == '0' || i3 == '0' || ( i2 == 'e' && i3 == 'h' ) || ( i2 == 'h' && i3 == 'e' ) )
	{
		codedInputs.push_back( i0 ); codedInputs.push_back( i1 );
	}
	else
		return false;

	UINT temp = codedInputs.size();
	char interMediate = codedInputs[ 0 ];
	interMediate = CalculateANDType( interMediate, codedInputs[ 1 ] );
	// Loop through inputs again and now detect fault
	char inherited = GetInheritedValue( fanouts, cov );
	for( UINT u = flag; u < ( flag + 2 ); ++u )
	{
		if( testedInputs[ u ] == 'R' )
			continue;
		if( cov[ faultNumbers[ u ] ] == 'R' && inherited == 'R' )
		{
			testedInputs[ u ] = 'R';
			continue;
		}

		char delay = '_';
		char temp = '_';
		if( flag == 0 )
			delay = DetectANDFault( codedInputs[ u ], interMediate );
		else 
			delay = DetectANDFault( codedInputs[ u - 2 ], interMediate );

		if( delay == 'X' && cov[ faultNumbers[ u ] ] == '_' )
			temp = 'X';
		else if( delay == 'N' && ( cov[ faultNumbers[ u ] ] == '_' || cov[ faultNumbers[ u ] ] == 'X' )  )
			temp = 'N';
		else if( delay == 'F' && ( cov[ faultNumbers[ u ] ] == '_' || cov[ faultNumbers[ u ] ] == 'X' ) )
			temp = 'F';
		else if( delay == 'R' )
			temp = 'R';
		else
			continue;
		
		if( temp == 'X' && inherited < temp )
			temp = inherited;
		else if( ( temp == 'N' && inherited == 'X' ) || ( temp == 'F' && inherited == 'X' ) )
			temp = 'X';
		else if( temp == 'R' && ( inherited < temp || temp == 'X' ) )
			temp = inherited;
		else if( ( temp == 'F' && inherited == 'N' ) || ( temp == 'N' && inherited == 'F' ) )
			temp = 'X';
		else if( temp == 'R' && inherited == 'R' )
			temp = 'R';

		testedInputs[ u ] = cov[ faultNumbers[ u ] ] = temp;
	}
	for( auto& tmp : testedInputs )
	{
		if( tmp != 'R' )  
			return false;
	}
	return true;
}


void CElementTestInfo::EncodeInputs( string& S1, string& S2 )
{
	for( auto tmp : testNumbers )
	{
		if( !Helper::IsHigh( S1[ tmp ] ) && !Helper::IsHigh( S2[ tmp ] ) )
			codedInputs.push_back( '0' );
		else if( !Helper::IsHigh( S1[ tmp ] ) && Helper::IsHigh( S2[ tmp ] ) )
			codedInputs.push_back( 'e' );
		else if( Helper::IsHigh( S1[ tmp ] ) && !Helper::IsHigh( S2[ tmp ] ) )
			codedInputs.push_back( 'h' );
		else
			codedInputs.push_back( '1' );
	}
}


const bool CElementTestInfo::IsTested( void ) const
{
	for( auto& tmp : testedInputs )
	{
		if( tmp != 'R' )
			return false;
	}
	return true;
}



const char CElementTestInfo::FindAOI2AND( const char c1, const char c2 )
{
	char tmp;

	if( c1 == '1' )
		tmp = c2;
	else if( c2 == '1' )
		tmp = c1;
	else if( c1 == '0' || c2 == '0' )
		tmp = '0';
	else if( c1 == 'X' || c2 == 'X' )
		tmp = 'X';
	else if( c1 == 'e' )
	{
		if( c2 == 'e' )
			tmp = 'e';
		else
			tmp = 'X';
	}
	else if( c1 == 'h' )
	{
		if( c2 == 'h' )
			tmp = 'h';
		else
			tmp = 'X';
	}

	return tmp;
}


const char CElementTestInfo::FindAOI2OR( const char c1, const char c2 )
{
	char tmp;

	if( c1 == '0' )
		tmp = c2;
	else if( c2 == '0' )
		tmp = c1;
	else if( c1 == '1' || c2 == '1' )
		tmp = '1';
	else if( c1 == 'X' || c2 == 'X' )
		tmp = 'X';
	else if( c1 == 'e' )
	{
		if( c2 == 'e' )
			tmp = 'e';
		else
			tmp = 'X';
	}
	else if( c1 == 'h' )
	{
		if( c2 == 'h' )
			tmp = 'h';
		else
			tmp = 'X';
	}

	return tmp;
}


CElementTestInfo::CElementTestInfo( const CElementTestInfo& orig )
{
	isTested = orig.isTested;
	isRobustTested = orig.isRobustTested;
	graphNum = orig.graphNum;
	typeName = orig.typeName;
	faultNumbers = orig.faultNumbers;
	//testedInputs = orig.testedInputs;
	testNumbers = orig.testNumbers;
}

CElementTestInfo& CElementTestInfo::operator= ( const CElementTestInfo& orig )
{
    if ( this == &orig )
        return *this;
 
	isTested = orig.isTested;
	isRobustTested = orig.isRobustTested;
	graphNum = orig.graphNum;
	typeName = orig.typeName;
	faultNumbers = orig.faultNumbers;
	//testedInputs = orig.testedInputs;
	testNumbers = orig.testNumbers;

    return *this;
}

CElementTestInfo::CElementTestInfo( CElementTestInfo&& orig )
{
	isTested = orig.isTested;
	isRobustTested = orig.isRobustTested;
	graphNum = orig.graphNum;
	typeName = orig.typeName;
	faultNumbers = orig.faultNumbers;
	//testedInputs = orig.testedInputs;
	testNumbers = orig.testNumbers;
}

CElementTestInfo& CElementTestInfo::operator= ( CElementTestInfo&& orig )
{
	if( this != &orig )
	{
		isTested = orig.isTested;
		isRobustTested = orig.isRobustTested;
		graphNum = orig.graphNum;
		typeName = orig.typeName;
		faultNumbers = orig.faultNumbers;
		//testedInputs = orig.testedInputs;
		testNumbers = orig.testNumbers;
	}
	return *this;
}


CElementTestInfo::~CElementTestInfo( void )
{
}
