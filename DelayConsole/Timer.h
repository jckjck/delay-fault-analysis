#pragma once

#include <windows.h>

// STL HEADERS
#include <iostream>
#include <string>

class Timer
{

private:
	
	// Primitives
	bool started;
	double PCFreq;
	__int64 counterStart;
	__int64 counterStop;

	// Containers
	std::string timerName;


public:
	
	void StartCounter( void );
	void StopCounter( void );
	void PrintResult( void );

	Timer( const std::string& name );
	~Timer( void );

};

