#include "CDataExtractor.h"

using std::cout;
using std::streamoff;
using std::replace;
using std::vector;
using std::tuple;
using std::string;
using std::istringstream;
using std::ifstream;
using std::ofstream;
using std::istream;
using std::endl;
using std::exception;
using std::ios;
using std::unique_ptr;
using std::shared_ptr;
using std::stringstream;
using std::map;
using std::move;
using std::make_pair;
using std::to_string;

/* READ RELATED FUNCTIONS */

bool CDataExtractor::IsGoodExt( const string& file, const string& ext )
{
	string s = file.substr( file.find_last_of( "." ) + 1 );
	UINT sz = s.size();

	if( ext.size() != sz )
	{
		cout << "Wrong file extension length!" << endl;
		return false;
	}

	for( size_t i = 0; i < sz; ++i )
	{
		if ( tolower( s[ i ] ) != tolower( ext[ i ] ) )
		{
			cout << "Wrong file extension!" << endl;
			return false;
		}
	}
	return true;
};


bool CDataExtractor::TryOpenInStream( void )
{
	try
	{
		m_pIn.open( m_FileName.c_str(), ios::in );
		m_pIn.exceptions( ifstream::eofbit | ifstream::failbit | ifstream::badbit );
		if( !m_pIn && !m_pIn.is_open() )
		{
			cout << "File Is NOT Open!" << endl;
			return false;
		}
	}
	catch( exception const& e )
	{
		string strFormatted = "The file " + m_FileName + " could not be opened due to this error: ";
		strFormatted += e.what();
		cout << strFormatted << endl;
		m_pIn.close();
		return false;
	}
	return true;
}


void CDataExtractor::ReadStat( vector< UINT >& stat )
{
	string line;	

	FindBlockPos( "STAT#" );
	
	getline ( m_pIn, line );

	string token;
	istringstream iss( line );
	while ( iss >> token )
	{
		if( IsNumber( token ) )
			stat.push_back( move( UINT( stoi( token ) ) ) );
	}
}


void CDataExtractor::ReadVars( vector< tuple< UINT, char, const string > >& vars )
{
	string line;
	FindBlockPos( "VAR#" );

	while( getline( m_pIn, line ) )
	{
		if( line.find( "VAR#" ) == string::npos )
			continue;

		istringstream iss( line );
		
		vars.push_back( move( ExtractVarData( iss ) ) );
	}
	m_pIn.seekg( m_pIn.beg );//It is faster here to return carriage
}


tuple< UINT, char, const string > CDataExtractor::ExtractVarData( istringstream& is )
{
	UINT u;
	string name, s;
	char c;

	while( is >> s )
	{
		if( s.find( ':' ) != string::npos ) 
		{
			s.pop_back();
			u = stoi( s );
		}
		else if( s.front() == '(' )
		{
			if( s.find( "(i_" ) ) c = 'i';
			else if( s.find( "(_o" ) ) c = 'o';
			else c = 'm';
		}
		else if( s.front() == '"' )
		{
			s.erase( 0, 1 );
			s.pop_back();
			name = s;
		}
	}
	return move( tuple< UINT, char, const string >( u, c, s ) );
}
	

void CDataExtractor::ExtractGraphParam( CGraph& grp )
{
	string s, line;

	getline( m_pIn, line );
	istringstream iss( line );

	while ( iss >> s )
	{	
		if( s.back() == ':' ) 
		{
			s.pop_back();
			grp.SetGrpNum( stoi( s ) );
		}
		else if( s.back() == ',' )
		{
			s.pop_back();
			grp.SetGrpBegin( stoi( s ) );
		}
		else if( IsNumber( s ) )
		{
			grp.SetGrpLength( stoi( s ) );
			break;
		}
	}  //It is faster here to return carriage
}


void CDataExtractor::ExtractNode( CGraph& ptr, bool st, string& line, UINT u, 
								 map< UINT, vector< UINT > >& mp )
{
	string s;
	UINT v = 0;

	istringstream iss( line );

	CNode nod;
	// Get global Node number from stream
	iss >> s;
	UINT tabVal = stoi( s );
	nod.SetFaultVal( tabVal );
	// Get Local node number in current string
	iss >> s;
	s.pop_back();
	nod.SetNodeNum( stoi( s ) );
	// Get inversion character
	iss >> s;
	s[ 1 ] == 'I' ? ( nod.ToggleInversion() ) : 0;
	// Get Node's downward successor number
	iss >> s;
	iss >> s;
	v = stoi( s );
	nod.SetDChild( v );
	// Get Node's rightward successor number	
	iss >> s;
	s.pop_back();
	nod.SetRChild( stoi( s ) );
	iss >> s;
	iss >> s;
	iss >> s;
	// Get mapping between pattern and table
	UINT m = stoi( s );
	nod.SetVarNum( m );
	auto it = mp.find( m );
	if( it == mp.end() )
	{
		vector< UINT > vec;
		vec.push_back( tabVal );
		mp.insert( make_pair( m, move( vec ) ) );
	}
	else
	{
		it->second.push_back( tabVal );
	}
	ptr.m_Graph.push_back( move( nod ) );
}


void CDataExtractor::ReadNextGraph( UINT nr, bool needSt, map< UINT, vector< UINT > >& mp, 
									vector< CGraph >& graphs )
{
	// Make search pattern for last input VAR
	string pattern = "VAR#   " + Uint2String( nr ) + ":";
	// String to read lines to and new CGraph object
	string line;	
	CGraph grp;
	// Node counter for current graph
	UINT counter = 0;
	// Get position of last var
	FindBlockPos( pattern );	
	getline( m_pIn, line );
	grp.SetEndNode( nr );
	ExtractGraphParam( grp );

	while( getline( m_pIn, line ) )
	{
		if( !( line.find( "VAR#" ) == string::npos ) || line.empty() )
			break;

		if( !( line.find( "CELL#" ) == string::npos ) )
		{
			string s;
			istringstream iss( line );
			// Get element type description from string stream iss
			iss >> s;
			iss >> s;
			grp.SetGrpType( s );
			continue;
		}

		ExtractNode( grp, needSt, line, counter, mp );
	}
	graphs.push_back( move( grp ) );
}


const UINT CDataExtractor::ReadVecCount( void )
{
	string line;	
	UINT cnt = 0;
	
	FindBlockPos( ".VECTORS" );
	
	getline ( m_pIn, line );

	string s;
	istringstream iss( line );

	while ( iss >> s )
	{
		if( IsNumber( s ) )
			cnt = stoi( s );
	}

	return cnt;
}


void CDataExtractor::ReadVectors( vector< string >& vec, const string& type )
{
	string line;	
	
	FindBlockPos( type );
	
	// TODO: Must be robust to different number of empty lines

	getline ( m_pIn, line );
	getline ( m_pIn, line );

	while ( getline ( m_pIn, line ) )
	{
		if( line.empty() )
			break;
		vec.push_back( line );
	}
}


void CDataExtractor::ReadVectors( vector< string >& vec, const string& type,
	const bool sec )
{
	string line;	
	
	FindBlockPos( type );
	
	// TODO: Must be robust to different number of empty lines

	getline ( m_pIn, line );
	getline ( m_pIn, line );
	getline ( m_pIn, line );

	while ( getline ( m_pIn, line ) )
	{
		if( line.empty() || line[ 0 ] == '-' )
			break;
		// Get every second
		getline ( m_pIn, line );
		vec.push_back( line );
	}
}


// Read and reduce vector count according to 5-valued algebra
void CDataExtractor::ReadVectors( vector< string >& vec, const string& type, const UINT out )
{
	string line1, line2;
	FindBlockPos( type );

	getline( m_pIn, line1 );
	getline( m_pIn, line1 );

	while ( getline( m_pIn, line1 ) )
	{
		// Assuming there is even number of vectors in the line
		if( line1.empty() )
			break;
		
		getline( m_pIn, line2 );
		string combined( line1.length(), '_' );

		CombineStrings( line1, line2, combined );

		vec.push_back( combined );
	}
}


const UINT CDataExtractor::ReadVectorsToPairs( vector< string >& vec, const string& type )
{
	string line;
	FindBlockPos( type );
	
	getline( m_pIn, line );
	getline( m_pIn, line );
	getline( m_pIn, line );

	vec.push_back( line );
	UINT count = 1;

	while( getline ( m_pIn, line ) )
	{
		// Assuming there is even number of vectors in the line
		if( line.empty() )
			break;

		vec.push_back( line );
		vec.push_back( line );
		count += 2;
	}
	vec.pop_back();
	return --count;
}


void CDataExtractor::CombineOutputs( const string& S1, const string& S2, 
								   string& res, const UINT sz )
{
	UINT total = S1.length();
	UINT begin = total - sz;

	for( UINT u = begin; u < total; ++u )
	{
		if( !Helper::IsHigh( S1[ u ] ) && !Helper::IsHigh( S2[ u ] ) )
			res[ u ] =  '0';
		else if( !Helper::IsHigh( S1[ u ] ) && Helper::IsHigh( S2[ u ] ) )
			res[ u ] =  'e';
		else if( Helper::IsHigh( S1[ u ] ) && !Helper::IsHigh( S2[ u ] ) )
			res[ u ] =  'h';
		else
			res[ u ] =  '1';
	}
}


void CDataExtractor::CombineStrings( const string& S1, const string& S2, string& res )
{
	UINT size = S1.size();
	for( UINT u = 0; u < size; ++u )
	{
		if( !Helper::IsHigh( S1[ u ] ) && !Helper::IsHigh( S2[ u ] ) )
			res[ u ] =  '0';
		else if( !Helper::IsHigh( S1[ u ] ) && Helper::IsHigh( S2[ u ] ) )
			res[ u ] =  'e';
		else if( Helper::IsHigh( S1[ u ] ) && !Helper::IsHigh( S2[ u ] ) )
			res[ u ] =  'h';
		else
			res[ u ] =  '1';
	}
}


void CDataExtractor::ReadFaultString( string& s )
{
	string line;

	FindBlockPos( ".FAULTS" );

	getline ( m_pIn, line );
	getline ( m_pIn, line );
	getline ( m_pIn, line );
	
	s = line;	
}


void CDataExtractor::FindBlockPos( const string& s )
{
	string line;
	bool flag = false;

	m_pIn.seekg( 0 );//inefficient!!!

	while( getline( m_pIn, line ) )
	{
		if( line.find( s ) != string::npos )
		{
			m_pIn.seekg( m_pIn.tellg() - ( pos_type )line.length() );
			flag = true;
			break;
		}
	}

	if( m_pIn.eof() && flag )
		cout << "String does not exist in that file" << endl;
}


string CDataExtractor::Uint2String( UINT num )
{
   stringstream ss;//create a stringstream
   ss << num;//add number to the stream

   return ss.str();//return a string with the contents of the stream
}


/* WRITE RELATED FUNCTIONS */

const int CDataExtractor::WritePairGroups( const map< string, vector< string > >& pairs )
{
	for( auto it = pairs.begin(); it != pairs.end(); ++it )
	{
		m_pOut << it->first + " *" << endl;
		for( auto pr = it->second.begin(); pr != it->second.end(); ++pr )
			m_pOut << *pr << endl;
	}
	// TODO: Add return logic
	return 0;
}


const int CDataExtractor::WriteNewTestFile( const vector< string >& pairs )
{
	// Write vec count and newline
	m_pOut << endl << ".VECTORS " << pairs.size() << endl << endl;
	// Write table name
	m_pOut << ".PATTERNS" << endl;
	// Write out generated testpairs
	for( auto it = pairs.begin(); it != pairs.end(); ++it )
		m_pOut << *it << endl;
	// Close output stream
	m_pOut.close();
	// TODO: Add return logic
	return pairs.size();
}


bool CDataExtractor::TryOpenOutStream( const string& file )
{
	try
	{
		m_pOut.open( file.c_str(), ios::out );
		m_pOut.exceptions( ofstream::out | ofstream::trunc );
		if( !m_pOut.is_open() )
		{
			cout << "File Is NOT Open!" << endl;
			return false;
		}
	}
	catch( exception const& e )
	{
		string strFormatted = "The file " + file + " could not be opened due to this error: ";
		strFormatted += e.what();
		cout << strFormatted << endl;
		m_pOut.close();
		return false;
	}
	return true;
}


void CDataExtractor::PrintTableHeader( const TABLEDATA& dat )
{
	// Calculate overall table width
	totalTableWidth = dat.uRowHead + dat.uIns + dat.uIntern + dat.uOuts;
	// Print delimiter line
	PrintDelimiter( m_pOut, totalTableWidth, 0 );
	// Print out row header part
	PrintHeaderRowPart( m_pOut, dat.uRowHead );
	// Print out textual parts
	PrintTextInCell( m_pOut, dat.uIns, "INPUTS" );
	PrintTextInCell( m_pOut, dat.uIntern, "INTERNAL NODES/VARIABLES" );
	PrintTextInCell( m_pOut, dat.uOuts + 2, "OUTPUTS" );
	m_pOut << endl;
	// And closing delimiter
	PrintDelimiter( m_pOut, totalTableWidth, 0 );
}


void CDataExtractor::PrintDelimiter( ofstream& out, const UINT width, const UINT spaces ) const 
{
	out << '|';
	// Add leading spaces
	for( UINT u = 0; u < spaces; ++u )
		out << ' ';
	// Print out given number of minuses
	for( UINT u = spaces; u < width - 2; ++u )
		out << '-';
	out << '|';
	// ...And endline
	out << endl;
}


void CDataExtractor::PrintHeaderRowPart( std::ofstream& out, const UINT width ) const
{
	out << '|';
	// Print width - 2 spaces
	for( UINT u = 0; u < ( width - 2 ); ++u )
		out << ' ';
	out << '|';
}


void CDataExtractor::PrintTextInCell( std::ofstream& out, const UINT width, 
									 const string& text ) const
{
	// Calculate spacec before and after text
	UINT space = ( width - 1 - text.length() ) / 2;
	// Check if space is in reasonable limits
	if( space > 1000 )
	{
		cout << "Strange space value in fuction PrintTextInCell" << endl;
		return;
	}
	// Spaces before...
	for( UINT u = 0; u < space; ++u ) 
		out << ' ';
	// ...text...
	out << text;
	// ...and spaces after
	for( UINT u = 0; u < space; ++u ) 
		out << ' ';
	out << '|';
}


void CDataExtractor::PrintTableData( 
	const TABLEDATA& dat, 
	map< UINT, UINT >& mp,
	vector< int >& prs, 
	vector< string >& test, 
	vector< string >& faults )
{
	bookMark = 0;
	UINT sz = prs.size();
	realSize = mp.size();
	// Print Test Tab header
	PrintTestHeader( dat );
	// Form table data for all testgroups
	for( UINT u = 0; u < sz; ++u )
	{
		// If there is no pairs for given vector...
		if( prs[ u ] == -1 ) continue;
		// Else print testgroups
		PrintTestGroups( dat, test, u, prs );
	}
	// Print Fault Tab header
	PrintFaultHeader( dat, mp );
	// Form table data for all fault vector groups
	//for( UINT u = 0; u < sz; ++u )
	//{
	//	// If there is no pairs for given vector...
	//	if( prs[ u ] == -1 ) continue;
	//	// Else print testgroups
	//	PrintTestGroups( dat, test, u, prs );
	//}
	bookMark = 0;
}


void CDataExtractor::PrintTestGroups( const TABLEDATA& dat, vector< string >& tests, 
									 const UINT group, vector< int >& prs )
{
	int temp = bookMark, k = 0;
	// Form string for testgroup header and print it out
	string groupText = "|  Test group " + to_string( group + 1 ) + "    |";
	m_pOut << groupText;
	// For every vector...
	for( int i = bookMark, j = 0; i < ( prs[group ] * 2 ) + temp; ++i, ++j, ++bookMark )
	{
		if( i % 2 ) AddTrailingSpaces( m_pOut, 0, groupText.length() - 1 );
		if( j % 2 ) ++k;
		// Print out vector itself divided into parts
		if( !j || j % 2 ) // Either main or secondary
		{
			// Form string for vector identifier and print it out
			string TNum = "T" + to_string( group + 1 ) + to_string( k );
			PrintTextInCell( m_pOut, dat.uRowHead - groupText.length(), TNum );
			PrintOutTestBits( dat, tests[ i ] );
			if( i != ( prs[ group ] * 2 ) + temp - 1 )
				PrintDelimiter( m_pOut, totalTableWidth, groupText.length() - 2 );
		}
	}
	// And group closing delimiter
	PrintDelimiter( m_pOut, totalTableWidth, 0 );
}


void CDataExtractor::PrintOutTestBits( const TABLEDATA& dat, string& vec )
{
	int i = 0; 
	UINT j = 0;
	// Print out inputs
	m_pOut << " " ;
	for( i = 1, j = 0; j < dat.uInput; ++j, i += 2 )
		m_pOut << vec[ j ] << " ";
	AddTrailingSpaces( m_pOut, i, dat.uIns - 2 );
	// Print out inputs
	m_pOut << " " ;
	for( i = 1; j < realSize - dat.uOutput; ++j, i += 6 )
		m_pOut << vec[ j ] << "     ";
	AddTrailingSpaces( m_pOut, i, dat.uIntern - 2 );
	// Print out Outputs
	m_pOut << " " ;
	for( i = 1 ; j < realSize; ++j, i += 3 )
		m_pOut << char( toupper( vec[ j ] ) ) << "  ";
	AddTrailingSpaces( m_pOut, i, dat.uOuts + 1 );
	m_pOut << endl;
}


void CDataExtractor::PrintTestHeader( const TABLEDATA& dat )
{
	UINT u = 0, i = 0;
	// Print out row headers text
	m_pOut << '|';
	PrintTextInCell( m_pOut, dat.uRowHead, "TEST TABLE (VARIABLES)" );
	// Print out inputs
	m_pOut << " " ;
	for( i = 1; u < dat.uInput; ++u, i += 2 )
		m_pOut << u << " ";
	AddTrailingSpaces( m_pOut, i, dat.uIns - 2 );
	// Print out inputs
	m_pOut << " " ;
	for( i = 1 ; u < realSize - dat.uOutput; ++u, i += 6 )
		m_pOut << u << "     ";
	AddTrailingSpaces( m_pOut, i, dat.uIntern - 2 );
	// Print out Outputs
	m_pOut << " " ;
	for( i = 1 ; u < realSize; ++u, i += 2 )
		m_pOut << u << " ";
	AddTrailingSpaces( m_pOut, i, dat.uOuts - 1 );
	// Print out endline and closing delimiter
	m_pOut << endl;
	PrintDelimiter( m_pOut, totalTableWidth, 0 );
}


void CDataExtractor::PrintFaultHeader( const TABLEDATA& dat, std::map< UINT, UINT >& mp )
{
	UINT u = 0, i = 0;
	// Print out row headers text
	m_pOut << '|';
	PrintTextInCell( m_pOut, dat.uRowHead, "FAULT TABLE (NODES) " );
	// Print out inputs
	m_pOut << " " ;
	for( i = 1; u < dat.uInput; ++u, i += 2 )
		m_pOut << mp[ u ] << " ";
	AddTrailingSpaces( m_pOut, i, dat.uIns - 2 );
	// Print out inputs
	m_pOut << " " ;
	for( i = 1 ; u < realSize - dat.uOutput; ++u, i += 6 )
		m_pOut << mp[ u ] << "     ";
	AddTrailingSpaces( m_pOut, i, dat.uIntern - 2 );
	// Print out Outputs
	m_pOut << " " ;
	for( i = 1 ; u < realSize; ++u, i += 2 )
		m_pOut << mp[ u ] << " ";
	AddTrailingSpaces( m_pOut, i, dat.uOuts - 1 );
	// Print out endline and closing delimiter
	m_pOut << endl;
	PrintDelimiter( m_pOut, totalTableWidth, 0 );
}


const std::string CDataExtractor::CreateNumString( 
	map< UINT, UINT >& mp, 
	const UINT start,
	const UINT width, 
	const int mode )
{
	string numString = " ";

	// TODO: To Finish
	return numString;
}


void CDataExtractor::AddTrailingSpaces( ofstream& out, const UINT cur, const UINT len )
{
	// Get local sizes
	UINT sz = cur;
	UINT length = len;
	// If spaces are meant to beginning, add pipe and shorten length for one character
	if( !cur )
	{
		out << '|';
		--length;
	}
	// Check correct inputs
	if( cur > length )
	{
		cout << "Incorrect unsigned size in AddTrailingSpaces";
		return;
	}
	// Add trailing spaces to get length same as sample
	while( sz != length )
	{
		out << " ";
		++sz;
	}
	// Add pipe
	out << '|';
}


//HRESULT IsNumber ( TCHAR *lpszInput, DWORD dwFlags )
//{
//   int        iSize       = lstrlen(lpszInput)+1;
//   OLECHAR    *lpwszInput = new OLECHAR[iSize*2];
//   UCHAR      *rgbDig     = new UCHAR[iSize];
//   NUMPARSE   numprs;
//   HRESULT    hRes;
//
//
//#ifdef _UNICODE
//   lstrcpy(lpwszInput, lpszInput);;
//#else
//   MultiByteToWideChar( CP_ACP, MB_PRECOMPOSED, lpszInput, iSize, lpwszInput, iSize );
//#endif
//
//   numprs.cDig = iSize;
//   numprs.dwInFlags = dwFlags;
//
//   hRes = VarParseNumFromStr(lpwszInput, GetUserDefaultLCID(), dwFlags, &numprs, rgbDig);
//
//   delete [] lpwszInput;
//   delete [] rgbDig;
//   return hRes;
//}

