
#include "Circuit.h"
#include <windows.h>


using std::string;
using std::wstring;
using std::vector;
using std::unique_ptr;
using std::shared_ptr;
using std::cout;
using std::endl;
using std::move;
using std::make_pair;
using std::map;
using std::set;



const bool CCircuit::DelayVectorsJob( const string& vects, const string& grp )
{
	if( !OpenFiles( vects, grp ) ) // TODO: If file is open check must be done if it consist information
		return false;
	SetGraphStats();
	SetGraphs( false );
	PrepareStrings( vects, grp );

	GeneratePairs();
	//GenereatePairsFromSAFTest();

	cout << "_delay.tst Created!" << endl;
	// Make for Devadze tool a copy of agm file with the same name as new tst file
	CopyFile( grp.c_str(), newAgm.c_str(), false );
	cout << "File _delay.agm created" << endl;
	// Call Devadze Tool first time to get SAF for created pairs
	if( !CallDevadzeTool( callString3.c_str() ) )
		return false;
	// Create Graph->Variables mapping
	GenerateTestInfo( m_CVect->GetTestArray() );
	// Format pair based results 
	m_CVect->GeneratePairAnalysis( newTst, pairsDsv, m_Var2Nod, timers );
	return true;
}


const bool CCircuit::CallDevadzeTool( LPCSTR param )
{
	// Create SHELLEXECUTEINFO pointer and allocate memory
	SHELLEXECUTEINFO* ShExecInfo = NULL;
	if( !( ShExecInfo = new SHELLEXECUTEINFO() ) )
	{
		cout << "Could not alloc memory for SHELLEXECUTEINFO struct" << endl;
		return false;
	}	
	// Fill created struct with data
	ShellDevadze( param, ShExecInfo );
	// Call Devadze tool to create fault table in new test file...
	ShellExecuteEx( ShExecInfo );
	// ...Wait to get back focus and free memory
	WaitForSingleObject( ShExecInfo->hProcess, INFINITE );
	delete ShExecInfo;

	return true;
}


void CCircuit::ShellDevadze( LPCSTR param, SHELLEXECUTEINFO* ShExecInfo )
{
	// Fill SHELLEXECUTEINFO bitfield with data
	ShExecInfo->cbSize = sizeof( SHELLEXECUTEINFO );
	ShExecInfo->fMask = SEE_MASK_NOCLOSEPROCESS;
	ShExecInfo->hwnd = NULL;
	ShExecInfo->lpVerb = NULL;
	ShExecInfo->lpFile = "C:\\WINDOWS\\system32\\cmd.exe";		
	ShExecInfo->lpParameters = param;	
	ShExecInfo->lpDirectory = NULL;
	ShExecInfo->nShow = SW_HIDE;
	ShExecInfo->hInstApp = NULL;	
}


void CCircuit::GenerateTestInfo( vector< unique_ptr< CElementTestInfo > >& testData )
{
	for( auto itg = m_Graphs.begin(); itg != m_Graphs.end(); ++itg )
	{
		// Create new unique pointer to CElementTestInfo object and fill it 
		unique_ptr< CElementTestInfo > ptr( new CElementTestInfo() );
		ptr->SetGraphNum( itg->GetGrpNum() );
		ptr->SetTypeName( itg->GetGrpType() );
		ptr->SetOutput( itg->GetEndNode() );
		// Loop through all the nodes in graph and get input numbers
		for( auto itn = itg->m_Graph.begin(); itn != itg->m_Graph.end(); ++itn )
		{
			// Get fault vector value
			UINT tmp = itn->GetFaultVal();
			// Insert next fault vector value to according vector
			ptr->SetInput( tmp );
			// Find fault vector value and according testvector value
			for( auto itm = m_Var2Nod.begin(); itm != m_Var2Nod.end(); ++itm )
			{
				// If found, insert found value to according vector
				if( std::find( itm->second.begin(), itm->second.end(), tmp ) != itm->second.end() )
					ptr->SetTest( itm->first );
			}
		}
		ptr->SetTested( ptr->GetInputSIze() );
		testData.push_back( move( ptr ) );
	}
}


CDataExtractor::TABLEDATA& CCircuit::FillTableData( CDataExtractor::TABLEDATA& dat )
{
	// Assign graph related sizes
	dat.uGrp = m_uGrpCount;
	dat.uVars = m_uVarCount;
	dat.uInput = m_uInCount;
	dat.uOutput = m_uOutCount;
	// Assign constants
	dat.uRowHead = rowHeaderWidth;
	// Calculate table size parameters
	dat.uIns = ( m_uInCount * 2 ) + 6;
	dat.uIntern = ( m_uVarCount * 2 ) + 6;
	dat.uOuts = ( m_uOutCount * 2 ) + 6;	
	return dat;
}


void CCircuit::SetGraphStats( void )
{
	vector< UINT > tmp;
	// Read graph parameters from file to vector
	m_DXgrp->ReadStat( tmp );
	// Set graph parameters
	SetNodCount( tmp.at( 0 ) );
	SetVarCount( tmp.at( 1 ) );
	SetGrpCount( tmp.at( 2 ) );
	SetInCount ( tmp.at( 3 ) );
	SetConCount( tmp.at( 4 ) );
	SetOutCount( tmp.at( 5 ) );
	// Set graph vector to exact size to avoid realloc later
	// m_Graphs.resize( GetGrpCount() );
}


void CCircuit::SetGraphs( const bool needStack )
{
	UINT u = GetInCount();
	UINT sz = u + GetGrpCount();

	//Read variable data

	while( u < sz )
	{
		m_DXgrp->ReadNextGraph( u++, needStack, m_Var2Nod, m_Graphs );
	}	
	
	// Extract all fanouts
	GenerateFanoutMap();
}


void CCircuit::GenerateFanoutMap( void )
{
	// Find all assotiations that have more than one mapings - these are fanouts
	for( auto itm = m_Var2Nod.begin(); itm != m_Var2Nod.end(); ++itm )
	{
		// If found, insert maping to new fanout map
		if( itm->second.size() > 1 )
			fanouts.insert( *itm );
	}
}


void CCircuit::SetVectors( void )
{
	SetVecCount( m_DXvec->ReadVecCount() );

	m_DXvec->ReadVectors( m_CVect->GetVecAccess( 0 ), ".PATTERNS" );

	m_DXvec->ReadVectors( m_CVect->GetVecAccess( 1 ), ".TABLE" );

	// TODO: Read Faults and coverage!!!
}


void CCircuit::GeneratePairs( void )
{
	UINT vecCount = 0;
	// Create vector objet for analysis and pair creation
	m_CVect.reset( new CVector( GetInCount(), GetOutCount(), GetVarCount() ) );
	// Read patterns and fault from file to created Vector object
	SetVectors();
	UINT sz = GetVecCount(); 
	// Create new timer and start it
	Timer timer( "D-Pair Generation" );
	timer.StartCounter();
	// For every vector, call pair generation
	for( UINT u = 0; u < sz; ++u )
		vecCount += m_CVect->GeneratePairSet( m_Var2Nod, pairs, m_Graphs );
	// Stop timer
	timer.StopCounter();
	// Save timer in next free place
	timers.push_back( timer );
	// Write out to newly named tst file all the pairs generated
	std::ofstream os( newTst, std::ios::binary | std::ios::out );
	os << "\r\n.VECTORS " << vecCount << "\r\n\r\n\r\n.PATTERNS\r\n\r\n";
	//m_CVect->MergeVectors( pairs, mergedPairs, 2, m_Graphs );
	// Write out pairs
	for( auto it1 = pairs.cbegin(); it1 != pairs.cend(); ++it1 )
	{
		for( auto& str : it1->second )
		{
			os << it1->first << "\r\n";
			os << str << "\r\n";
		}
	}

	//for( auto& it : mergedPairs )
	//{
	//	os << it << "\r\n";
	//}
}


void CCircuit::GenereatePairsFromSAFTest( void )
{
	// Create vector objet for analysis and pair creation
	m_CVect.reset( new CVector( GetInCount(), GetOutCount(), GetVarCount() ) );
	m_DXvec->ReadVectors( m_CVect->GetVecAccess( 1 ), ".TABLE" );
	// Create new timer and start it
	Timer timer( "D-Pair Generation" );
	timer.StartCounter();
	// Read SAF Vectors to pairs
	UINT vecCount = m_DXvec->ReadVectorsToPairs( m_CVect->GetVecAccess( 0 ), ".PATTERNS" );	
	timer.StopCounter();
	// Save timer in next free place
	timers.push_back( timer );
	// Write out to newly named tst file all the pairs generated
	std::ofstream os( newTst, std::ios::binary | std::ios::out );
	os << "\r\n.VECTORS " << vecCount << "\r\n\r\n\r\n.PATTERNS\r\n\r\n";
	// Write out pairs
	for( auto& str : m_CVect->GetVecAccess( 0 ) )
		os << str << "\r\n";
	os.close();
}


const bool CCircuit::OpenFiles( const string& v, const string& g )
{
	if( !CDataExtractor::IsGoodExt( v, "tst" ) && !CDataExtractor::IsGoodExt( g, "agm" ) )
		return false;

	m_DXvec.reset( new CDataExtractor( v ) );
	m_DXgrp.reset( new CDataExtractor( g ) );

	if( !m_DXvec->TryOpenInStream() || !m_DXgrp->TryOpenInStream() )
		return false;

	return true;
}


const string CCircuit::MakeNewFileName( const string& oldName, 
									   const string& ext, const bool addExt )
{
	string name = oldName;
	// Remove extension
	for( int i = 0; i < 4; ++i )
		name.pop_back();
	// If ext is TRUE, add suffix and extension..
	if( addExt )
		return name + "_delay." + ext;
	// Else add only suffix
	else
		return name + "_delay";
}


void CCircuit::PrepareStrings( const string& tst, const string& agm )
{
	// Create tst file with additional _delay mnemonic
	newTst = MakeNewFileName( tst, "tst", true );
	// Creat strings for copying agm files
	newAgm = MakeNewFileName( agm, "agm", true );
	// Make callstring based on newTst
	// Create new file name
	string call = MakeNewFileName( tst, "tst", false );
	// Add call prefix and escape sequence
	callString = "/c \"npsimul3.exe " + call + "\"";
	callString2 = "/c \"npsimul3.exe " + call + "\"";
	callString3 = "/c \"npsimul3.exe -fault_graphs " + call + "\"";	
	// Create delay analysis result file 
	pairsDsv = MakeNewFileName( tst, "csv", true );
	// Create table file name 
	tableFile = MakeNewFileName( tst, "tbl", true );
}


CCircuit::CCircuit( void )
{
}

CCircuit::~CCircuit(void)
{
}

// COOL FEATURE
 //   FILE* pipe = _popen("npsimul3.exec432", "r");
 //   if (!pipe) 
	//	AfxMessageBox( _T("Problem") );
 //   char buffer[128];
 //   std::string result = "";
 //   while(!feof(pipe)) 
	//{
 //   	if(fgets(buffer, 1024, pipe) != NULL)
 //   		result += buffer;
	//}
 //  
 //   _pclose(pipe);

	//AfxMessageBox( CString( result.c_str() ) );