#include "stdafx.h"
#include <iostream>
#include "CGRaphRoutines.h"

using std::cout;

//bool CGraphRoutines::FormInitialGraphSet( void )
//{
//	m_pGrp->FormGraph( &m_vecMain, &m_stkLNodes );
//	bool formed = ( m_vecMain.size() ? TRUE : FALSE );
//
//	formed = formed && m_pGrp->FillGraph( &m_vecTest );
//
//	m_pGrp->GetGraphStat();
//	formed = ( m_SGraphStat.uGrpNum ? ( formed && TRUE ) : ( formed && FALSE ) );
//
//	return formed;
//}

//bool CGraphRoutines::FormInitialVectorSet( void )
//{
//	m_pVec->ReadVectorData();
//
//	bool vc = ( ( m_uVecCount = m_pVec->GetVecCount() ) == 
//		m_vecMain.size() ) ? TRUE : FALSE;
//	bool tv = ( ( m_vecTestVectors = m_pVec->GetVectors() ).size() );
//
//	return vc && tv;
//}

//void CGraphRoutines::SetNodeValues( const int mode, const CString& values,
//								   vector< CNode* >& grp )
//{
//	int j = 0;
//	for( auto i = grp.begin(); i != grp.end(); ++i, ++j )
//	{
//		if( IsHigh( values.GetAt( j ) ) )
//		{
//			( *i )->ToggleOne();
//			( *i )->ClearZero();
//			if( mode ) ( *i )->ToggleInversion();
//		}
//		else
//		{
//			( *i )->ToggleZero();
//			( *i )->ClearOne();
//		}
//	}
//}
//
//const bool CGraphRoutines::IsHigh( TCHAR tc ) const 
//{
//	return ( tc == 'l' || tc == 'L' || tc == '1' );
//}
//
//void CGraphRoutines::InvertGraph( vector< CNode* >& grp )
//{
//	for( auto i = grp.begin(); i != grp.end(); ++i )
//	{
//		( *i )->ToggleInversion();
//
//		CNode* tmp = ( *i )->GetDChild();//create outside of cycle ??
//		( *i )->SetDChild( ( *i )->GetRChild() );
//		( *i )->SetRChild( tmp );
//	}
//}
//
//const int CGraphRoutines::FaultFreeSimul( const CNode* cn ) const
//{
//	if( !cn->IsInverted() )
//	{
//		if( cn->IsOne() )
//			return ( !cn->GetRChild() ? 1 : FaultFreeSimul( cn->GetRChild() ) );
//		else 
//			return ( !cn->GetDChild() ? 0 : FaultFreeSimul( cn->GetDChild() ) );
//	}
//	else
//	{
//		if( cn->IsOne() )
//			return ( !cn->GetDChild() ? 0 : FaultFreeSimul( cn->GetDChild() ) );
//		else 
//			return ( !cn->GetRChild() ? 1 : FaultFreeSimul( cn->GetRChild() ) );
//	}
//	//throw unknown error
//}
//
//void CGraphRoutines::MultiplyGraph( vector< CNode* >& newgrp )
//{
//	m_ulCurNodeCount += m_ulInitNodeCount;
//
//	while( !m_stkLNodes.empty() )
//	{
//		m_stkLNodes.top()->SetRChild( newgrp.at( 0 ) );
//		m_stkLNodes.pop();
//	}	
//
//	UINT num = m_vecMain.size();
//	for( auto& s: newgrp )
//	{
//		s->SetVarNum( num++ );
//		m_vecMain.push_back( s );
//	}
//
//	newgrp.clear(); //is it safe?
//}
//
//void CGraphRoutines::TestNextVector( vector< CNode* >& newgrp, const CString& vec )
//{
//	SetNodeValues( 0, vec, m_vecTest );
//	bool b = FaultFreeSimul( m_vecTest.at( 0 ) ) == vec.GetAt( vec.GetLength() - 1 );
//
//	if( m_uRoundCount++ )
//	{
//		SetNodeValues( 1, vec, newgrp );
//		if( b ) InvertGraph( newgrp );
//		MultiplyGraph( newgrp );
//	}
//	else
//	{
//		SetNodeValues( 1, vec, m_vecMain );
//		if( b ) InvertGraph( m_vecMain );
//	}
//	ReduceGraph();
//}
//
//const int CGraphRoutines::GetNodeType( const CNode* cn ) const
//{
//	if( !cn->GetRChild() && ! cn->GetDChild() )
//		return 1;
//	else if ( cn->GetDChild() )
//		return cn->GetRChild() ? 2 : 3;
//	else
//		return 4;
//}
//
//const int CGraphRoutines::CheckCollision( void )
//{
//	if( !m_vecColList.size() ) return 0;
//
//	for( auto& s: m_vecColList )
//	{
//		if( s->GetNodeNum() != m_pCur->GetNodeNum() ) continue;
//		
//		if( ( CNode::BothNotInv( s, m_pCur ) && !CNode::SameVal( s, m_pCur ) ) ||
//			( CNode::DifInvVal( s, m_pCur ) && CNode::SameVal( s, m_pCur ) ) )
//		{
//			//RemoveFromList();
//			return 1;
//		}
//	}
//	//AddToList();
//	return 0;
//}
//
//bool CGraphRoutines::Handle0Nod( void )
//{
//	m_vecColList.push_back( m_pCur );
//	return true;
//}
//
//bool CGraphRoutines::Hanlde0NodErr( void )
//{
//	return true;
//}
//
//void CGraphRoutines::ReduceGraph( void )
//{
//	m_pCur = m_vecMain.at( 0 );
//	int flag = 0;
//
//	while( 1 )
//	{
//		switch( GetNodeType( m_pCur ) )
//		{
//			case 1:
//				m_bContinue = CheckCollision();
//		}
//
//	}
//	//	while( 1 ) 
////	{
////		if( !( tmp->pRight ) )	
////		{
////			if( !tmp->pDown ) 
////			{
////				if( noCollision( scl, tmp ) ) 
////				{
////					attachToList( scl, tmp );
////					attachToGraph( sg, tmp, 0, newSt );
////					analyzePath( scl, sr );		
////				}
////				else
////					deleteFromGraph( msg, 0 );
////				if( !isStackEmpty( oldStack ) ) 
////				{
////					msg->curNode = popStack( oldStack );
////					msg->chDownward = TRUE;
////				}
////				else break;			
////			}
////			else 
////			{
////				if( noCollision( msg ) ) 
////				{
////					attachToGraph( msg, 1 );
////					attachToList( msg );
////					analyzePath( msg->collisionList, SRes, &ui, msg->curNode );
////					msg->collisionList[ --msg->uiListIndex ] = NULL;
////				}
////				else 
////					msg->chDir = RIGHT;
////				msg->curNode = msg->curNode->pDown;
////			}
////		}	
////		else 
////		{
////			if( msg->curNode->pDown ) 
////			{
////				if( !msg->chDownward ) 
////				{
////					if( noCollision( msg ) )	
////					{
////						pushStack( oldStack, msg->curNode );
////						attachToGraph( msg, 2 );
////						attachToList( msg );
////						msg->curNode = msg->curNode->pRight;
////					}
////					else 
////					{
////						msg->chDir = RIGHT;
////						msg->chDownward = TRUE;
////						msg->curNode = msg->curNode->pDown;
////					}
////				}	
////				else
////				{
////					//printf( "prep to delete\n" ); 
////					deleteFromList( msg->collisionList, msg->curNode, &msg->uiListIndex );
////					msg->curNode = msg->curNode->pDown;			
////					msg->chDownward = FALSE;
////				}
////			}
////			else 
////			{
////				if( noCollision( msg ) )	
////				{
////					attachToGraph( msg, 3 );
////					attachToList( msg );
////					msg->curNode = msg->curNode->pRight;
////				}
////				else 
////				{
////					msg->chDownward = TRUE;
////					if( !isStackEmpty( oldStack ) ) 
////						msg->curNode = popStack( oldStack );
////					else break;	
////					deleteFromGraph( msg, 0 );					
////				}
////			}
////		}
////	}
////	SRes->iPathCount = ui;
////}
//}
//
//void CGraphRoutines::AttachToGraph( CNode* cn, int mod )
//{
//	switch( mod )
//	{
//		case 0:
//			;
//	}
//}

//static SNode* attachToGraph( SMessage* msg, char chCase )
//{
//
//	switch( chCase )
//	{
//		case 0:
//			connectNode( msg, sn );
//			findNewActiveNode( msg );
//			break;
//		case 1:
//			connectNode( msg, sn );
//			sn->uVarFlags |= NOD_PRS;
//			msg->activeNode = sn;
//			msg->chActiveDir = DOWN;
//			msg->prevInsert = msg->lastInsert;
//			msg->lastInsert = sn;
//			msg->chDir = DOWN;
//			pushStack( msg->newStack, sn );
//			break;
//		case 2:
//			connectNode( msg, sn );
//			msg->activeNode = sn;
//			msg->chActiveDir = RIGHT;
//			msg->prevInsert = msg->lastInsert;
//			msg->lastInsert = sn;
//			msg->chDir = RIGHT;
//			pushStack( msg->newStack, sn );
//			break;
//		case 3:
//			connectNode( msg, sn );
//			msg->prevInsert = msg->lastInsert;
//			msg->lastInsert = sn;
//			msg->chDir = RIGHT;
//			break;
//		default:
//			//error message?
//			break;
//	}
//	return sn;
//}
///*****************************************************************************/
///*info*/


//static void connectNode( SMessage* msg, SNode* sn )
//{
//	msg->newGraph[ msg->uiCount ] = sn;
//	if( msg->lastInsert )
//	{/*printf("connected: %s to %s\n", sn->szNodeName, msg->lastInsert->szNodeName );*/
//		if( msg->chDir ) 
//			( msg->lastInsert->pRight = msg->newGraph[ msg->uiCount ] );
//		else
//			(  msg->lastInsert->pDown = msg->newGraph[ msg->uiCount ] );
//	}
//	msg->uiCount++;
//}
///*****************************************************************************/
///*info*/

CGraphRoutines::~CGraphRoutines( void )
{
}
