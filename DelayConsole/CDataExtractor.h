#pragma once

// CUSTOM HEADERS
#include "CGraph.h"
#include "ElementTestInfo.h"
#include "Helper.h"

#include <ctime>

// STL HEADERS
#include <vector>
#include <string>
#include <fstream>
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <algorithm> 
#include <tuple>
#include <memory>
#include <map>

typedef unsigned int UINT;
typedef unsigned long int ULONG;
typedef std::char_traits< char >::pos_type pos_type;

class CDataExtractor
{
public:

	// STRUCTS

	struct TIMERINFO
	{
		std::string timerName;
		clock_t startTime;
		clock_t endTime;
	};

	struct VARINFO
	{
		unsigned char byType;
		std::string sName;
		ULONG ulVarNum;
	};

	struct GRAPHDATA
	{
		UINT uLineNum;
		UINT uOrderNum;
		int iInverted;
		UINT uDown;
		UINT uRight;
		UINT uVarNum;
		std::string strVarName;
	};

	// Struct for passing data into Dataextractor for creating analysis table
	struct TABLEDATA
	{
		UINT uGrp;			// Graphs count 
		UINT uVars;			// Count of all variables
		UINT uInput;		// Input count of current graph
		UINT uOutput;		// Output count of current graph
		UINT uRowHead;		// Length of row header part in table
		UINT uIns;			// Length of input part in table 
		UINT uIntern;		// Length of internal part in table
		UINT uOuts;			// Length of output part of table
	};


private:

/* VARIABLES */

	// PRIMITIVES
	UINT totalTableWidth;	// Total with of analysis tabe
	UINT realSize;			// Real count of different variables in vector
	int bookMark;			// Variable for holding bookMark in vector selection

	// CONTAINERS
	std::string m_FileName;

	// MEMBER OBJECTS
	std::ifstream m_pIn;
	std::ofstream m_pOut;
	std::exception m_exFile;
	

/* METHODS */

	// Check if string contains only numbers
	inline const bool IsNumber( const std::string& s ) const
	{
		return !s.empty() && find_if( s.begin(), s.end(), 
			[]( char c ) { return !isdigit( c ); } ) == s.end();
	};

	// Print out delimiter line 
	void PrintDelimiter( std::ofstream& out, const UINT width, const UINT spaces ) const; 

	// Print out header row part
	void PrintHeaderRowPart( std::ofstream& out, const UINT width ) const;

	// Print out header input part
	void PrintTextInCell( std::ofstream& out, const UINT width, 
		const std::string& text ) const;

	// Print out Test Tab header data
	void PrintTestHeader( const TABLEDATA& dat );

	// Print out Fault Tab header data
	void PrintFaultHeader( const TABLEDATA& dat, std::map< UINT, UINT >& mp );

	// Create string for printing in cell from numbers
	const std::string CreateNumString( std::map< UINT, UINT >& mp, 
		const UINT start, const UINT width, const int mode );

	// Adds needed amount of spaces at the end of string
	void AddTrailingSpaces( std::ofstream& out, const UINT cur, const UINT len );

	// Print test groups discovering delay faults
	void PrintTestGroups( const TABLEDATA& dat, std::vector< std::string >& tests, 
		const UINT group, std::vector< int >& prs );

	// Print out testvector devided into three parts
	void PrintOutTestBits( const TABLEDATA& dat, std::string& vec );

	void FindBlockPos( const std::string& name );
	void ExtractGraphParam( CGraph& grp );
	void ExtractNode( CGraph& ptr, bool st, std::string& line, UINT u, 
		std::map< UINT, std::vector< UINT > >& mp );
	std::tuple< UINT, char, const std::string > ExtractVarData( std::istringstream& is );
	std::string Uint2String( UINT num );

	void CombineOutputs( const std::string& S1, const std::string& S2, 
		std::string& res, const UINT sz );

	void CombineStrings( const std::string& S1, const std::string& S2, 
		std::string& res );

	
	

public:

/* METHODS */

	// Construct/Destruct
	CDataExtractor( const std::string& fName ) : m_FileName( fName ){};

	~CDataExtractor( void )
	{
		//m_pIn.close();
		//m_pOut.close();
	};

	// Custom
	bool TryOpenInStream( void );
	bool TryOpenOutStream( const std::string& file );
	void ReadStat( std::vector< UINT >& stat );
	void ReadVars( std::vector< std::tuple< UINT, char, const std::string > >& vars );

	// Read next graph from agm file
	void ReadNextGraph( UINT nr, bool needSt, std::map< UINT, std::vector< UINT > >& mp, 
		std::vector< CGraph >& graphs );

	const UINT ReadVecCount( void );

	// Read All vectors from desired block
	void ReadVectors( std::vector< std::string >& vec, const std::string& type );

	// Read All vectors from desired block, coupled to pairs
	const UINT ReadVectorsToPairs( std::vector< std::string >& vec, const std::string& type );

	// Read and reduce vector count according to 5-valued algebra
	void ReadVectors( std::vector< std::string >& vec, const std::string& type, 
		std::vector< std::unique_ptr< CElementTestInfo > >& elem, const UINT out );

	// Read and reduce vector count according to 5-valued algebra
	void ReadVectors( std::vector< std::string >& vec, const std::string& type, const UINT out );

	// Read Every second vector from desired block
	void ReadVectors( std::vector< std::string >& vec, const std::string& type,
		const bool sec );

	void ReadFaultString( std::string& s );

	static bool IsGoodExt( const std::string& file, const std::string& ext );

	const int WritePairGroups( const std::map< std::string, 
		std::vector< std::string > >& pairs );

	// Generates new .tst file  for Devadze tool
	const int WriteNewTestFile( const std::vector< std::string >& pairs );

	// Write single line to outputstream
	inline void WriteLine( const std::string& line ) { m_pOut << line << '\n'; };

	// Read single line from inputstream
	inline std::string& ReadLine( std::string& line ) { m_pIn >> line; };	

	// Print out comparison table header
	void PrintTableHeader( const TABLEDATA& dat );

	// Print out all table data except header
	void PrintTableData( const TABLEDATA& dat, std::map< UINT, UINT >& mp,
		std::vector< int >& prs, std::vector< std::string >& test, 
		std::vector< std::string >& faults );

	// Getter for outstream
	inline std::ofstream& GetOutStream( void ) { return m_pOut; };

};

