#pragma once

#include "CNode.h"
#include "CGraph.h"
#include "CVector.h"
#include "CFileRoutines.h"
#include "CFaultTable.h"

#include <vector>
using std::vector;

#include <stack>
using std::stack;

class CGraphRoutines
{

public:

	explicit CGraphRoutines( CGraph* grp, /*CVector* vec,*/ CFaultTable* ft ) : 
	m_pGrp( grp ),/* m_pVec( vec ),*/ m_pFlt( ft )
	{
		m_SGraphStat.uGrpNum = 0;
	};
	//bool FormInitialGraphSet( void );
	bool FormInitialVectorSet( void );
	bool FormInitialFaultSet( void );
	const int FaultFreeSimul( const CNode* cn ) const;
	~CGraphRoutines(void);	

private:

	//MEMBER VARIABLES
		//general
		UINT m_uRoundCount;
		UINT m_uVecCount;
		ULONG m_ulInitNodeCount;
		ULONG m_ulCurNodeCount;
		//old graph
		bool m_bContinue;
		int m_iDirOld;
		//new graph
		int m_iDirNew;

	//CString strFaultsTested;

	//DATA STRUCTURES
		//general
		vector< CNode* > m_vecTest;
		vector< CNode* > m_vecColList;
		vector< CString > m_vecTestVectors;
		vector< CString > m_vecFaultTable;
		CFileRoutines::GRAPHSTAT m_SGraphStat;
		//old graph
		stack< CNode* > m_stkBOld;
		vector< CNode* > m_vecMain;
		//new graph
		stack< CNode* > m_stkLNodes;
		stack< CNode* > m_stkBNew;	

	//MEMBER OBJECTS
	CNode* m_pCur;
	CGraph* m_pGrp;
	CGraph* m_pNewGraph;
	//CVector* m_pVec;
	CFaultTable* m_pFlt;

	//METHODS	
	void SetNodeValues( const int mode, const CString& values, vector< CNode* >& grp );
	const bool IsHigh( const TCHAR tc ) const;
	void InvertGraph( vector< CNode* >& grp );
	const int GetNodeType( const CNode* cn ) const;
	void MultiplyGraph( vector< CNode* >& newgrp );
	void TestNextVector( vector< CNode* >& newgrp, const CString& vec );
	void ReduceGraph( void );
	void AttachToGraph( CNode* cn, int mod );
	const int CheckCollision( void );
	bool Handle0Nod( void );
	bool Hanlde0NodErr( void );
	//void RemoveFromList( 

};

