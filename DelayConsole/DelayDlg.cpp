
// DelayDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Delay.h"
#include "DelayDlg.h"
#include "afxdialogex.h"
#include "CGraph.h"
#include "CVector.h"
#include "Circuit.h"

#include <bitset> 
using std::bitset;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CDelayDlg dialog



CDelayDlg::CDelayDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDelayDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDelayDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edtFileName);
	DDX_Control(pDX, IDC_EDIT3, m_edtExtension);
	//DDX_Control(pDX, IDC_MYLABEL, m_lblResult);
	DDX_Control(pDX, IDC_LIST1, m_lstData);
}

BEGIN_MESSAGE_MAP(CDelayDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
//	ON_BN_CLICKED(IDOK, &CDelayDlg::OnBnClickedOk)
ON_BN_CLICKED(IDOK, &CDelayDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CDelayDlg message handlers

BOOL CDelayDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDelayDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDelayDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDelayDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CDelayDlg::OnBnClickedOk()
{
	clock_t t1 = GetTickCount();
	CCircuit cc;
	cc.DelayVectorsJob("c1355.tst", "c1355.agm");
	
	clock_t t2 = GetTickCount();
	clock_t time = t2 - t1;
	m_lstData.AddString( _T( "OK" ) );
	CString ts;
	ts.Format( _T( "%d" ), time );
	CString s = _T( "Process took: " ) + ts + _T( " ms" );	

	m_lstData.AddString( s );

	// Print out separate times
	for( UINT u = 0; u < cc.GetTimersCount(); ++u )
	{
		CString ss( cc.GetTimeInfo( u ).timerName.c_str() );

		ss += _T( ": " );

		CString st;
		st.Format( _T( "%d" ), ( cc.GetTimeInfo( u ).endTime - cc.GetTimeInfo( u ).startTime ) );
		ss += st;
		ss += _T( " ms" );

		m_lstData.AddString( ss );
	}

	//cc.PrintOutDelayComparison();

	// applyToolChain();
}


void CDelayDlg::applyToolChain(void)
{
	
}
