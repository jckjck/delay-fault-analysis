#pragma once

// CUSTOM HEADERS
#include "CNode.h"

// STL HEADERS
#include <vector>
#include <stack> 
#include <map>
#include <memory>
#include <string>

typedef unsigned int UINT;

class CGraph
{

private:
	
/* VARIABLES */

	// Primitives
	UINT m_uGrpNum;
	UINT m_uGrpBegin;
	UINT m_uGrpLen;
	UINT m_uEndNode;

	//Containers
	std::string elType;
	 
public:

/* VARIABLES */
	
	// CONTAINERS
	std::vector< CNode > m_Graph;

/* METHODS */

	// CONSTRUCT / DESTRUCT

	CGraph() :
		m_uGrpNum( 0 ), 
		m_uGrpBegin( 0 ), 
		m_uGrpLen( 0 ),
		m_uEndNode( 0 )
	{};
	// Move constructor
	CGraph( CGraph&& orig )
	{
		m_uGrpNum = orig.m_uGrpNum;
		m_uGrpBegin = orig.m_uGrpBegin;
		m_uGrpLen = orig.m_uGrpLen;
		m_uEndNode = orig.m_uEndNode;
		m_Graph = orig.m_Graph;
		elType = orig.elType;
	}
	// Move assignment operator
	CGraph& operator= ( CGraph&& orig )
	{
		if( this != &orig )
		{
			m_uGrpNum = orig.m_uGrpNum;
			m_uGrpBegin = orig.m_uGrpBegin;
			m_uGrpLen = orig.m_uGrpLen;
			m_uEndNode = orig.m_uEndNode;
			m_Graph = orig.m_Graph;
			elType = orig.elType;
		}
		return *this;
	}
	// Destructor
	~CGraph(){};

	// Custom
	const bool ReadAllData( bool st );
	void GetData( void );/*
	void ResizeGraph( UINT size );*/
	void SetNodeValues( const int mode, const std::string& values );
	const int FaultFreeSimul( std::vector< CNode >& nods ) const;
	

	//Getters
	const UINT GetGrpNum  ( void ) const { return m_uGrpNum;   };
	const UINT GetGrpBegin( void ) const { return m_uGrpBegin; };
	const UINT GetGrpLength( void ) const{ return m_uGrpLen;   };
	const UINT GetEndNode  ( void ) const{ return m_uEndNode;  };
	const std::string GetGrpType ( void ) const { return elType; };
	CNode* PopNodeStack( void );
	CNode GetNode( UINT num );

	//Setters
	void SetGrpNum  ( UINT num  ) { m_uGrpNum   = num;  };
	void SetGrpBegin( UINT beg  ) { m_uGrpBegin = beg;  };
	void SetGrpLength( UINT len ) { m_uGrpLen   = len;  };
	void SetEndNode  ( UINT end ) { m_uEndNode  = end;  };
	void SetGrpType ( std::string type ) { elType = type; };
	void PushNodeStack( CNode* ptr );


private:

/* VARIABLES */

	// Primitives

	// Containers
	
	//std::stack< CNode* > m_ConNodes;

/* METHODS */

	//CGraph( const CGraph& fileName ); //Disable copy constructor
	void SetGraphParams( void );
	const bool FillGraph( bool needStack );
	void AllocGraph( void );
};


