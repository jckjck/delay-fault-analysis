#pragma once

#include <string>

class Helper
{

public:

// VARIABLES


// METHODS

	// CUSTOM METHODS

	// Encode vectorpair outputs for 5 and 7 valued algebra
	static inline const bool IsDynamic( const char& out )
	{ return out ==' h' || out == 'e'; };

	// Static member for checking if bit is high in vector
	static inline const bool IsHigh( const char& ch ) 
		{ return ( ch == '1' || ch == 'h' || ch == 'H' ); };

	// Static member for checking if bit is SAF tested
	static inline const bool IsSAFTested( const char& ch ) 
		{ return ( ch != 'X' ); };

	// Static member for checking if bit is delay-tested
	static inline const bool IsDelayTested( const char& ch ) 
		{ return ( ch == 'X' || ch == 'F' || ch == 'N' || ch == 'R' ); };

	// Static member for checking if two bits are different and not 'X'
	static inline const bool IsDiff( const char& ch1, const char& ch2 ) 
		{ return ( ch1 != 'X' && ch2 != 'X' && ch1 != ch2 ); };

	// Convert double to string and replace dot with comma 
	static inline std::string DoubleToCSV( const double& d, const bool semiColon )
	{
		std::string dStr = std::to_string( d );

		for( auto& ch : dStr )
		{
			if( ch == '.' )
				ch = ',';
		}

		if( semiColon )
			dStr.push_back( ';' );

		return ( std::move( dStr ) );
	};

private:

	// To avoid creating instances of this class
	Helper(void);
	~Helper(void);

};

